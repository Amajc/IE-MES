package com.iemes.util;

/**
 * 列表的列对象，可设置列表的相关属性
 * @author Administrator
 *
 */
public class MTableCol {

	private String colName;
	
	private String colKey;
	
	private int hide = 1;		//1是显示，-1是隐藏
	
	public MTableCol(String colKey, String colName, int hide) {
		this.colName = colName;
		this.colKey = colKey;
		this.hide = hide;
	}
	
	public MTableCol(String colKey, String colName) {
		this.colName = colName;
		this.colKey = colKey;
	}

	public String getColName() {
		return colName;
	}

	public void setColName(String colName) {
		this.colName = colName;
	}

	public String getColKey() {
		return colKey;
	}

	public void setColKey(String colKey) {
		this.colKey = colKey;
	}

	public int getHide() {
		return hide;
	}

	public void setHide(int hide) {
		this.hide = hide;
	}
}
