package com.iemes.service.impl.WorkshopInventory;

import javax.inject.Inject;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.iemes.entity.WorkShopInventoryFormMap;
import com.iemes.exception.BusinessException;
import com.iemes.mapper.base.BaseMapper;
import com.iemes.service.WorkshopInventory.WorkshopInventoryReceptionService;
import com.iemes.util.UUIDUtils;

@Service
public class WorkshopInventoryReceptionServiceImpl implements WorkshopInventoryReceptionService {
	@Inject
	private BaseMapper baseMapper;
	
	private Logger log = Logger.getLogger(this.getClass());

	@Override
	@Transactional
	public void receptItem(WorkShopInventoryFormMap workshopInventoryFormMap) throws BusinessException {
		try {			
			workshopInventoryFormMap.put("id", UUIDUtils.getUUID());
			baseMapper.addEntity(workshopInventoryFormMap);
		} 	catch (Exception e) {
			log.error(e.getMessage(), e);
			throw new BusinessException(e.getMessage());
		}
	}
}
