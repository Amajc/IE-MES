package com.iemes.service.impl.workcenter_model;

import java.util.List;

import javax.inject.Inject;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import com.iemes.entity.ShopOrderSfcFormMap;
import com.iemes.entity.WorkCenterFormMap;
import com.iemes.entity.WorkResourceFormMap;
import com.iemes.entity.WorkResourceTypeFormMap;
import com.iemes.exception.BusinessException;
import com.iemes.mapper.WorkcenterModel.WorkResourceMapper;
import com.iemes.mapper.base.BaseMapper;
import com.iemes.service.workcenter_model.WorkResourceTypeService;
import com.iemes.util.Common;
import com.iemes.util.ListUtils;
import com.iemes.util.ShiroSecurityHelper;
import com.iemes.util.UUIDUtils;

@Service
public class WorkResourceTypeServiceImpl implements WorkResourceTypeService {
	
	@Inject
	private BaseMapper baseMapper;
	
	@Inject
	private WorkResourceMapper workResourceMapper;
	
	private Logger log = Logger.getLogger(this.getClass());

	@Override
	@Transactional
	public void saveWorkResourceType(WorkResourceTypeFormMap workResourceTypeFormMap) throws BusinessException {
		try {
			String id = workResourceTypeFormMap.getStr("id");
			String resourceType = workResourceTypeFormMap.getStr("resource_type");
			
			if(StringUtils.isEmpty(resourceType)) {
				throw new BusinessException("资源类型编号不能为空，请输入！");
			}	
			
			//如果编号存在，就执行更新，不存在就执行新增
			WorkResourceTypeFormMap workResourceTypeFormMap2 = new WorkResourceTypeFormMap();
			workResourceTypeFormMap2.put("site_id", ShiroSecurityHelper.getSiteId());
			workResourceTypeFormMap2.put("resource_type", resourceType);
			List<WorkResourceTypeFormMap> listWorkResourceTypeFormMap = baseMapper.findByNames(workResourceTypeFormMap2);
			if (ListUtils.isNotNull(listWorkResourceTypeFormMap)) {
				//更新 
				WorkResourceTypeFormMap workResourceTypeFormMap3 = new WorkResourceTypeFormMap();
				workResourceTypeFormMap3 = listWorkResourceTypeFormMap.get(0);
				id = workResourceTypeFormMap3.getStr("id");
				workResourceTypeFormMap.set("id", id);
				
				baseMapper.editEntity(workResourceTypeFormMap);
			}else {
				//新增
				id = UUIDUtils.getUUID();
				workResourceTypeFormMap.set("id",id);
				baseMapper.addEntity(workResourceTypeFormMap);
			}

			// unSelectedWorkResources
			// 更新关联资源
			String selectedWorkResources = workResourceTypeFormMap.getStr("selectedWorkResources");
			String unelectedWorkResources = workResourceTypeFormMap.getStr("unSelectedWorkResources");

			if (!Common.isEmpty(selectedWorkResources)) {
				String[] workResourcesIdArray = selectedWorkResources.split(",");
				for (String workResource_id : workResourcesIdArray) {
					WorkResourceFormMap workResourceFormMap = new WorkResourceFormMap();
					workResourceFormMap.put("id", workResource_id);
					workResourceFormMap.put("resource_type_id", id);
					baseMapper.editEntity(workResourceFormMap);
				}
			}
			if (!Common.isEmpty(unelectedWorkResources)) {
				String[] workResourcesIdArray = unelectedWorkResources.split(",");
				for (String workResource_id : workResourcesIdArray) {
					WorkResourceFormMap workResourceFormMap = new WorkResourceFormMap();
					workResourceFormMap.put("id", workResource_id);
					workResourceFormMap.put("resource_type_id", "");
					baseMapper.editEntity(workResourceFormMap);
				}
			}

		} catch (Exception e) {
			log.error(e.getMessage(), e);
			throw new BusinessException("资源类型保存失败！" + e.getMessage());
		}
	}

	@Override
	@Transactional
	public void delWorkResourceType(String workResourceTypeId) throws BusinessException {
		try {
			//1.判断资源类型是否有绑定的资源，如果有不允许删除
			WorkResourceFormMap workResourceFormMap = new WorkResourceFormMap();
			workResourceFormMap.put("resource_type_id", workResourceTypeId);
			List<WorkResourceFormMap> listWorkResourceFormMap = baseMapper.findByNames(workResourceFormMap);
			if(ListUtils.isNotNull(listWorkResourceFormMap)) {
				String bindWorkResourceNo = "";
				for(int i=0;i<listWorkResourceFormMap.size();i++) {
					WorkResourceFormMap map = listWorkResourceFormMap.get(i);
					if(StringUtils.isEmpty(bindWorkResourceNo)) {
						bindWorkResourceNo = map.getStr("resource_no");
					}else {
						bindWorkResourceNo += "," + map.getStr("resource_no");
					}
				}
				throw new BusinessException("此资源类型绑定了资源 " + bindWorkResourceNo + "，无法执行删除！");
			}
			
			// 2.删除资源类型表
			WorkResourceTypeFormMap workResourceTypeFormMap = new WorkResourceTypeFormMap();
			workResourceTypeFormMap.put("id", workResourceTypeId);
			baseMapper.deleteByNames(workResourceTypeFormMap);
			// 3.更新关联资源
			workResourceMapper.updateWorkResourceTypeIdToEmptyByWorkResourceTypeId(workResourceTypeId);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			throw new BusinessException("资源类型删除失败！" + e.getMessage());
		}
	}

}
