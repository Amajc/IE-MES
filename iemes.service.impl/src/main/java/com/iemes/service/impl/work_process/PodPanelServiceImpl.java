package com.iemes.service.impl.work_process;

import java.util.List;

import javax.inject.Inject;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import com.iemes.entity.PodPanelButtonFormMap;
import com.iemes.entity.PodPanelFormMap;
import com.iemes.entity.UserFormMap;
import com.iemes.exception.BusinessException;
import com.iemes.mapper.BaseExtMapper;
import com.iemes.service.work_process.PodPanelService;
import com.iemes.util.DateUtils;
import com.iemes.util.ListUtils;
import com.iemes.util.ShiroSecurityHelper;
import com.iemes.util.UUIDUtils;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

@Service
public class PodPanelServiceImpl implements PodPanelService {
	
	private Logger log = Logger.getLogger(this.getClass());
	
	@Inject
	private BaseExtMapper baseMapper;

	@Override
	@Transactional
	public void savePodPanel(PodPanelFormMap podPanelFormMap) throws BusinessException {
		try {
			String id = podPanelFormMap.getStr("id");
			String podPanelNo = podPanelFormMap.getStr("pod_panel_no");
			
			if(StringUtils.isEmpty(podPanelNo)) {
				throw new BusinessException("POD功能变化编号不能为空，请输入！");
			}
			
			PodPanelFormMap podPanelFormMap2 = new PodPanelFormMap();
			podPanelFormMap2.put("pod_panel_no", podPanelNo);
			podPanelFormMap2.put("site_id", ShiroSecurityHelper.getSiteId());
			List<PodPanelFormMap> list = baseMapper.findByNames(podPanelFormMap2);
			
			//如果编号存在，就执行更新，不存在就执行新增
			if (ListUtils.isNotNull(list)) {
				//更新 
				PodPanelFormMap podPanelFormMap3 = new PodPanelFormMap();
				podPanelFormMap3 = list.get(0);
				id = podPanelFormMap3.getStr("id");
				podPanelFormMap.set("id", id);
				
				baseMapper.editEntity(podPanelFormMap);
			}else {
				//新增
				podPanelFormMap.put("id", UUIDUtils.getUUID());
				baseMapper.addEntity(podPanelFormMap);
			}
			
			//先删除、再添加
			baseMapper.deleteByAttribute("pod_panel_id", podPanelFormMap.getStr("id"), PodPanelButtonFormMap.class);
			String buttons = podPanelFormMap.getStr("buttons");
			JSONArray buttonObj = JSONArray.fromObject(buttons);
			for (int i=0;i<buttonObj.size();i++) {
				JSONObject button = (JSONObject) buttonObj.get(i);
				PodPanelButtonFormMap podPaneButtonFormMap = new PodPanelButtonFormMap();
				podPaneButtonFormMap.put("pod_panel_button_id", ((JSONArray)((JSONObject)button.get("pod_botton_no")).get("value")).get(1));
				podPaneButtonFormMap.put("pod_panel_button_level", ((JSONArray)((JSONObject)button.get("pod_botton_level")).get("value")).get(0));
				podPaneButtonFormMap.put("pod_panel_id", podPanelFormMap.getStr("id"));
				podPaneButtonFormMap.put("site_id", ShiroSecurityHelper.getSiteId());
				podPaneButtonFormMap.put("id", UUIDUtils.getUUID());
				baseMapper.addEntity(podPaneButtonFormMap);
			}
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			throw new BusinessException("POD面板保存失败！"+e.getMessage());
		}
	}

	@Override
	@Transactional
	public void delPodPanel(String podPanelId) throws BusinessException {
		try {
			baseMapper.deleteByAttribute("id", podPanelId, PodPanelFormMap.class);
			
			baseMapper.deleteByAttribute("pod_panel_id", podPanelId, PodPanelButtonFormMap.class);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			throw new BusinessException("POD面板删除失败！"+e.getMessage());
		}
	}

}
