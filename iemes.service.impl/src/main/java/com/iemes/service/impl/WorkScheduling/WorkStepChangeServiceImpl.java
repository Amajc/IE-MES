package com.iemes.service.impl.WorkScheduling;

import java.util.List;

import javax.inject.Inject;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.iemes.entity.SfcStepFormMap;
import com.iemes.entity.ShopOrderSfcFormMap;
import com.iemes.entity.ShoporderFormMap;
import com.iemes.entity.StepChangeHistoryFormMap;
import com.iemes.exception.BusinessException;
import com.iemes.mapper.base.BaseMapper;
import com.iemes.service.WorkScheduling.WorkStepChangeService;
import com.iemes.util.DateUtils;
import com.iemes.util.ShiroSecurityHelper;
import com.iemes.util.UUIDUtils;

@Service
public class WorkStepChangeServiceImpl implements WorkStepChangeService {
	@Inject
	private BaseMapper baseMapper;

	private Logger log = Logger.getLogger(this.getClass());

	@Override
	@Transactional
	public void changeWorkStep(SfcStepFormMap sfcStepFormMapBeforeChange, ShopOrderSfcFormMap shopOrderSfcFormMap,
			ShopOrderSfcFormMap shopOrderSfcFormMapInDb,ShoporderFormMap shoporderFormMap,
			List<ShopOrderSfcFormMap> listShopOrderSfcFormMapOfOneShoporder,
			int newOperationStatus)
			throws BusinessException {
		try {
			String stepIdBeforeChange = sfcStepFormMapBeforeChange.getStr("id");
			String sfcId = sfcStepFormMapBeforeChange.getStr("shoporder_sfc_id");
			String sfcNo = sfcStepFormMapBeforeChange.getStr("sfc");
			String sfcNewOperationId = shopOrderSfcFormMap.getStr("new_operation_id");
			String sfcStepOperationIdBeforeChange = sfcStepFormMapBeforeChange.getStr("operation_id");
			String remark = shopOrderSfcFormMap.getStr("remark");
			boolean isAllFlowStepDone = false;
			
			//1.更新上一个步骤的finish_time
			sfcStepFormMapBeforeChange.set("finish_time", DateUtils.getStringDateTime());
			baseMapper.editEntity(sfcStepFormMapBeforeChange);
			
			String stepIdAfterChange = "";
			//2.把上个操作增加已完成的记录mds_sfc_step
			SfcStepFormMap sfcStepFormMap = new SfcStepFormMap();
		    stepIdAfterChange = UUIDUtils.getUUID();
			sfcStepFormMap.put("id", stepIdAfterChange);
			sfcStepFormMap.put("sfc", sfcNo);
			sfcStepFormMap.put("process_workflow_id", sfcStepFormMapBeforeChange.getStr("process_workflow_id"));
			sfcStepFormMap.put("operation_id", sfcStepOperationIdBeforeChange);
			sfcStepFormMap.put("status", 2);
			sfcStepFormMap.put("workline_id", sfcStepFormMapBeforeChange.getStr("workline_id"));
			sfcStepFormMap.put("shoporder_id", sfcStepFormMapBeforeChange.getStr("shoporder_id"));
			sfcStepFormMap.put("site_id", sfcStepFormMapBeforeChange.getStr("site_id")); 
			sfcStepFormMap.put("create_time", DateUtils.getStringDateTime());
			sfcStepFormMap.put("create_user", ShiroSecurityHelper.getCurrentUsername());
			
			//增加 work_resource_id item_id shift_id workshop_id shoporder_sfc_id
			sfcStepFormMap.put("work_resource_id", sfcStepFormMapBeforeChange.getStr("work_resource_id"));
			sfcStepFormMap.put("item_id", sfcStepFormMapBeforeChange.getStr("item_id"));
			sfcStepFormMap.put("shift_id", sfcStepFormMapBeforeChange.getStr("shift_id"));
			sfcStepFormMap.put("workshop_id", sfcStepFormMapBeforeChange.getStr("workshop_id"));
			sfcStepFormMap.put("shoporder_sfc_id", sfcId);
			//增加备注
			sfcStepFormMap.put("remark", "[车间作业步骤调整]：" + remark);

			baseMapper.addEntity(sfcStepFormMap);

			//3.如果不是调到结束标志“END” 则 1.新增mds_sfc_step记录
			if (!sfcNewOperationId.equals("STEP_END")) {
				SfcStepFormMap sfcStepFormMap2 = new SfcStepFormMap();
				stepIdAfterChange = UUIDUtils.getUUID();
				sfcStepFormMap2.put("id", stepIdAfterChange);
				sfcStepFormMap2.put("sfc", sfcNo);
				sfcStepFormMap2.put("process_workflow_id", sfcStepFormMapBeforeChange.getStr("process_workflow_id"));
				sfcStepFormMap2.put("operation_id", sfcNewOperationId);
				sfcStepFormMap2.put("status", 0);
				sfcStepFormMap2.put("workline_id", sfcStepFormMapBeforeChange.getStr("workline_id"));
				sfcStepFormMap2.put("shoporder_id", sfcStepFormMapBeforeChange.getStr("shoporder_id"));
				sfcStepFormMap2.put("site_id", sfcStepFormMapBeforeChange.getStr("site_id"));
				sfcStepFormMap2.put("create_time", DateUtils.getStringDateTime());
				sfcStepFormMap2.put("create_user", ShiroSecurityHelper.getCurrentUsername());

				// 增加 work_resource_id item_id shift_id workshop_id shoporder_sfc_id
				sfcStepFormMap2.put("work_resource_id", sfcStepFormMapBeforeChange.getStr("work_resource_id"));
				sfcStepFormMap2.put("item_id", sfcStepFormMapBeforeChange.getStr("item_id"));
				sfcStepFormMap2.put("shift_id", sfcStepFormMapBeforeChange.getStr("shift_id"));
				sfcStepFormMap2.put("workshop_id", sfcStepFormMapBeforeChange.getStr("workshop_id"));
				sfcStepFormMap2.put("shoporder_sfc_id", sfcId);

				baseMapper.addEntity(sfcStepFormMap2);
			}else {
				isAllFlowStepDone = true;
			}
			
			//4.如果是跳到最后一个结束状态，则还需要去看sfc和工单那边的状态是否需要更新，
			if(isAllFlowStepDone) {
			    //把sfc的状态设置为已完成，还要看工单的状态
				//3.1 修改sfc表状态为2（已完成）
				shopOrderSfcFormMapInDb.put("sfc_status", 2);
				baseMapper.editEntity(shopOrderSfcFormMapInDb);
				
				//3.2判断是否为工单的最后一个，是则修改工单状态为已完成
				int num = shoporderFormMap.getInt("shoporder_number");
				if (listShopOrderSfcFormMapOfOneShoporder.size() + 1==num) {
					shoporderFormMap.put("status", 3);
					baseMapper.editEntity(shoporderFormMap);
				}
			}		
			
			// 5.新增调整历史记录
			StepChangeHistoryFormMap stepChangeHistoryFormMap = new StepChangeHistoryFormMap();
			stepChangeHistoryFormMap.put("id", UUIDUtils.getUUID());
			stepChangeHistoryFormMap.put("sfc_id", sfcId);
			stepChangeHistoryFormMap.put("step_id_before_change", stepIdBeforeChange);
			stepChangeHistoryFormMap.put("step_id_after_change", stepIdAfterChange);
			stepChangeHistoryFormMap.put("change_remark", remark);
			stepChangeHistoryFormMap.put("change_time", DateUtils.getStringDateTime());
			stepChangeHistoryFormMap.put("change_user", ShiroSecurityHelper.getCurrentUsername());
			stepChangeHistoryFormMap.put("create_time", DateUtils.getStringDateTime());
			stepChangeHistoryFormMap.put("create_user", ShiroSecurityHelper.getCurrentUsername());

			baseMapper.addEntity(stepChangeHistoryFormMap);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			throw new BusinessException(e.getMessage());
		}
	}

}
