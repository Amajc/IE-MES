package com.iemes.service.impl.workcenter_model;

import java.util.List;

import javax.inject.Inject;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import com.iemes.entity.SfcStepFormMap;
import com.iemes.entity.ShiftFormMap;
import com.iemes.entity.UDefinedDataValueFormMap;
import com.iemes.entity.UserFormMap;
import com.iemes.entity.UserShiftFormMap;
import com.iemes.exception.BusinessException;
import com.iemes.mapper.base.BaseMapper;
import com.iemes.service.CommonService;
import com.iemes.service.workcenter_model.ShiftService;
import com.iemes.util.Common;
import com.iemes.util.DateUtils;
import com.iemes.util.ListUtils;
import com.iemes.util.ShiroSecurityHelper;
import com.iemes.util.UUIDUtils;

@Service
public class ShiftServiceImpl implements ShiftService {
	@Inject
	private BaseMapper baseMapper;
	
	@Inject
	private CommonService commonService;
	
	private Logger log = Logger.getLogger(this.getClass());

	@Override
	@Transactional
	public void saveShift(ShiftFormMap shiftFormMap) throws BusinessException {
		try {			
			String id = shiftFormMap.getStr("id");
			String shiftNo = shiftFormMap.getStr("shift_no");

			if(StringUtils.isEmpty(shiftNo)) {
				throw new BusinessException("班次编号不能为空，请输入！");
			}
			
			ShiftFormMap shiftFormMap2 = new ShiftFormMap();
			shiftFormMap2.put("site_id", ShiroSecurityHelper.getSiteId());
			shiftFormMap2.put("shift_no", shiftNo);
			List<ShiftFormMap> listShiftFormMap = baseMapper.findByNames(shiftFormMap2);
			
			//如果编号存在，就执行更新，不存在就执行新增
			if (ListUtils.isNotNull(listShiftFormMap)) {
				//更新 
				ShiftFormMap shiftFormMap3 = new ShiftFormMap();
				shiftFormMap3 = listShiftFormMap.get(0);
				id = shiftFormMap3.getStr("id");
				shiftFormMap.set("id", id);
				
				baseMapper.editEntity(shiftFormMap);
			}else {
				//新增
				id = UUIDUtils.getUUID();
				shiftFormMap.set("id", id);
				baseMapper.addEntity(shiftFormMap);
			}
			
			// 2.添加或更新用户的班次信息
			UserShiftFormMap userShiftFormMap = new UserShiftFormMap();
			userShiftFormMap.put("shift_id", id);
			baseMapper.deleteByNames(userShiftFormMap);
			String selectedUserList = shiftFormMap.getStr("selectedUserList");
			if (!Common.isEmpty(selectedUserList)) {
				String[] userListArray = selectedUserList.split(",");
				for (String user_id : userListArray) {
					userShiftFormMap.put("id", UUIDUtils.getUUID());
					userShiftFormMap.put("site_id", ShiroSecurityHelper.getSiteId());
					userShiftFormMap.put("shift_id", id);
					userShiftFormMap.put("user_id", user_id);
					userShiftFormMap.put("create_user", ShiroSecurityHelper.getCurrentUsername());
					userShiftFormMap.put("create_time", DateUtils.getStringDateTime());

					baseMapper.addEntity(userShiftFormMap);
				}
			}	
			// 3.添加或更新自定义数据信息
			String dataValue = shiftFormMap.getStr("udefined_data");
			commonService.saveUDefinedDataValueS(id, dataValue);
			
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			throw new BusinessException("班次保存失败！" + e.getMessage());
		}
	}

	@Override
	@Transactional
	public void delShift(String shiftId) throws BusinessException {
		try {
			// 1.如果班次已用于生产，则不允许删除
			SfcStepFormMap sfcStepFormMap = new SfcStepFormMap();
			sfcStepFormMap.put("shift_id", shiftId);
			List<SfcStepFormMap> listSfcStepFormMap= baseMapper.findByNames(sfcStepFormMap);
			if (ListUtils.isNotNull(listSfcStepFormMap)) {
				throw new BusinessException("此班次已用于生产，不可删除！");
			}
			
			// 2.删除班次表
			ShiftFormMap shiftFormMap = new ShiftFormMap();
			shiftFormMap.put("id", shiftId);
			baseMapper.deleteByNames(shiftFormMap);

			// 3.删除用户班次表
			UserShiftFormMap userShiftFormMap = new UserShiftFormMap();
			userShiftFormMap.put("shift_id", shiftId);
			baseMapper.deleteByNames(userShiftFormMap);
			
			// 4.删除自定义数据表
			 UDefinedDataValueFormMap uDefinedDataValueFormMap = new UDefinedDataValueFormMap();
			 uDefinedDataValueFormMap.put("data_type_detail_id",shiftId);
			 baseMapper.deleteByNames(uDefinedDataValueFormMap);

		} catch (Exception e) {
			log.error(e.getMessage(), e);
			throw new BusinessException("班次删除失败！" + e.getMessage());
		}
	}

}
