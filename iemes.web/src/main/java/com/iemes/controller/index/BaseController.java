package com.iemes.controller.index;

import java.util.ArrayList;
import java.util.Enumeration;

import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;

import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.iemes.entity.ResFormMap;
import com.iemes.entity.UDefinedDataFieldFormMap;
import com.iemes.entity.UDefinedDataFormMap;
import com.iemes.entity.UDefinedDataValueFormMap;
import com.iemes.entity.UserFormMap;
import com.iemes.mapper.ResourcesMapper;
import com.iemes.mapper.base.BaseMapper;
import com.iemes.plugin.PageView;
import com.iemes.util.Common;
import com.iemes.entity.FormMap;
import com.iemes.util.ListUtils;
import com.iemes.util.ShiroSecurityHelper;

/**
 * 
 * @author MDS
 */
public class BaseController {
	@Inject
	private ResourcesMapper resourcesMapper;
	
	@Inject
	protected BaseMapper baseMapper;
	
	public PageView pageView = null;
	public PageView getPageView(String pageNow,String pageSize,String orderby) {
		if (Common.isEmpty(pageNow)) {
			pageView = new PageView(1);
		} else {
			pageView = new PageView(Integer.parseInt(pageNow));
		}
		if (Common.isEmpty(pageSize)) {
			pageSize = "10";
		} 
		pageView.setPageSize(Integer.parseInt(pageSize));
		pageView.setOrderby(orderby);
		return pageView;
	}
	
	public PageView getPageView(String pageNow,String pageSize) {
		if (Common.isEmpty(pageNow)) {
			pageView = new PageView(1);
		} else {
			pageView = new PageView(Integer.parseInt(pageNow));
		}
		if (Common.isEmpty(pageSize)) {
			pageSize = "10";
		} 
		pageView.setPageSize(Integer.parseInt(pageSize));
		return pageView;
	}
	
	//逻辑分页
	public void LuogicPaging(String pageNow, String pageSize, List<Map<String, Object>> list) throws Exception {
		int page = Integer.parseInt(pageNow == null ? "1" : pageNow);
		int intPageSize = Integer.parseInt(pageSize == null ? "10" : pageSize); 
		pageView = new PageView(page);
		pageView.setPageSize(intPageSize);

		int start = (page - 1) * pageView.getPageSize();
		int end = page * pageView.getPageSize();
		end = end > list.size() ? list.size() : end;

		List<Map<String, Object>> rs = new ArrayList<Map<String, Object>>();
		for (int i = start; i < end; i++) {
			rs.add(list.get(i));
		}
		pageView.setRowCount(list.size());
		pageView.setRecords(rs);
	}
	
	public <T> T toFormMap(T t,String pageNow,String pageSize,String orderby){
		@SuppressWarnings("unchecked")
		FormMap<String, Object> formMap = (FormMap<String, Object>) t;
		formMap.put("paging", getPageView(pageNow, pageSize,orderby));
		return t;
	}
	
	/**
	 * 获取返回某一页面的按扭组,
	 * <br/>
	 *<b>author：</b><br/> 
	 *<b>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsplanyuan</b><br/> 
	 *<b>date：</b><br/> 
	 *<b>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp2015-04-01</b><br/> 
	 *<b>mod by：</b><br/> 
	 *<b>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbspEkko</b><br/> 
	 *<b>date：</b><br/> 
	 *<b>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp2015-09-07</b><br/> 
	 *<b>version：</b><br/> 
	 *<b>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp3.0v</b>
	 * @return Class<T>
	 * @throws Exception
	 */
	public List<ResFormMap> findByRes(){
		// 资源ID
		String id = getPara("id");
		// 获取request
		HttpServletRequest request = ((ServletRequestAttributes)RequestContextHolder.getRequestAttributes()).getRequest();  
		// 通过工具类获取当前登录的bean
		UserFormMap userFormMap = (UserFormMap)Common.findUserSession(request);
		// user id
		String userId = userFormMap.getStr("id");
		ResFormMap resQueryForm = new ResFormMap();
		resQueryForm.put("parentId", id);
		resQueryForm.put("user_id", userId);
		List<ResFormMap> rse = resourcesMapper.findRes(resQueryForm);
		//List<ResFormMap> rse = resourcesMapper.findByAttribute("parentId", id, ResFormMap.class);
		for (ResFormMap resFormMap : rse) {
			Object o =resFormMap.get("description");	
			if(o!=null&&!Common.isEmpty(o.toString())){
				resFormMap.put("description",Common.stringtohtml(o.toString()));
			}
		}
		return rse;
	}
	
	public void handlePageRes(Model model,HttpServletRequest request) {
		String resId = request.getParameter("page_res_id");
		if(StringUtils.isEmpty(resId)) {
			resId = getPara("id");
		}
		model.addAttribute("page_res_id", resId);
		model.addAttribute("res", findButtonByResByParentResId(resId));
	}
	
	public List<ResFormMap> findButtonByResByParentResId(String parentResId){
		// 获取request
		HttpServletRequest request = ((ServletRequestAttributes)RequestContextHolder.getRequestAttributes()).getRequest();  
		// 通过工具类获取当前登录的bean
		UserFormMap userFormMap = (UserFormMap)Common.findUserSession(request);
		List<ResFormMap> rse = new ArrayList<ResFormMap>();
		// user id
		String userId = userFormMap.getStr("id");
		if (userId.equals("0")) {
			ResFormMap resFormMap = getFormMap(ResFormMap.class);
			resFormMap.put("where", " where 1=1 and parent_id='"+ parentResId +"' order by level asc");
			rse = resourcesMapper.findByWhere(resFormMap);
		} else {
			ResFormMap resQueryForm = new ResFormMap();
			resQueryForm.put("parentId", parentResId);
			resQueryForm.put("user_id", userId);
			rse = resourcesMapper.findRes(resQueryForm);
		}
		//List<ResFormMap> rse = resourcesMapper.findByAttribute("parentId", id, ResFormMap.class);
		for (ResFormMap resFormMap : rse) {
			Object o =resFormMap.get("description");	
			if(o!=null&&!Common.isEmpty(o.toString())){
				resFormMap.put("description",Common.stringtohtml(o.toString()));
			}
		}
		return rse;
	}
	
	public List<ResFormMap> findByResByParentResId(String parentResId){
		// 资源ID
		String id = getPara("id");
		// 获取request
		HttpServletRequest request = ((ServletRequestAttributes)RequestContextHolder.getRequestAttributes()).getRequest();  
		// 通过工具类获取当前登录的bean
		UserFormMap userFormMap = (UserFormMap)Common.findUserSession(request);
		List<ResFormMap> rse = new ArrayList<ResFormMap>();
		// user id
		String userId = userFormMap.getStr("id");
		if (userId.equals("0")) {
			ResFormMap resFormMap = getFormMap(ResFormMap.class);
			resFormMap.put("where", " where 1=1 order by level asc");
			rse = resourcesMapper.findByWhere(resFormMap);
		} else {
			ResFormMap resQueryForm = new ResFormMap();
			resQueryForm.put("parentId", parentResId);
			resQueryForm.put("user_id", userId);
			rse = resourcesMapper.findRes(resQueryForm);
		}
		//List<ResFormMap> rse = resourcesMapper.findByAttribute("parentId", id, ResFormMap.class);
		for (ResFormMap resFormMap : rse) {
			Object o =resFormMap.get("description");	
			if(o!=null&&!Common.isEmpty(o.toString())){
				resFormMap.put("description",Common.stringtohtml(o.toString()));
			}
		}
		return rse;
	}
	
	/**
	 * 获取页面传递的某一个参数值,
	 * <br/>
	 *<b>author：</b><br/> 
	 *<b>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsplanyuan</b><br/> 
	 *<b>date：</b><br/> 
	 *<b>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp2015-04-01</b><br/> 
	 *<b>version：</b><br/> 
	 *<b>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp3.0v</b>
	 */
	public String getPara(String key){
		HttpServletRequest request = ((ServletRequestAttributes)RequestContextHolder.getRequestAttributes()).getRequest();  
		return request.getParameter(key);
	}
	
	/**
	 * 获取页面传递的某一个数组值,
	 * <br/>
	 *<b>author：</b><br/> 
	 *<b>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsplanyuan</b><br/> 
	 *<b>date：</b><br/> 
	 *<b>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp2015-04-01</b><br/> 
	 *<b>version：</b><br/> 
	 *<b>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp3.0v</b>
	 * @return Class<T>
	 * @throws Exception
	 */
	public String[] getParaValues(String key){
		HttpServletRequest request = ((ServletRequestAttributes)RequestContextHolder.getRequestAttributes()).getRequest();  
		return request.getParameterValues(key);
	}
	
	/**
	 * 获取传递的所有参数,
	 * 反射实例化对象，再设置属性值
	 * 通过泛型回传对象.
	 * <br/>
	 *<b>author：</b><br/> 
	 *<b>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsplanyuan</b><br/> 
	 *<b>date：</b><br/> 
	 *<b>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp2015-04-01</b><br/> 
	 *<b>version：</b><br/> 
	 *<b>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp3.0v</b>
	 * @return Class<T>
	 * @throws Exception
	 */
	public <T> T getFormMap(Class<T> clazz){
		HttpServletRequest request = ((ServletRequestAttributes)RequestContextHolder.getRequestAttributes()).getRequest();  
		Enumeration<String> en = request.getParameterNames();
		T t = null;
		try {
			t = clazz.newInstance();
			@SuppressWarnings("unchecked")
			FormMap<String, Object> map = (FormMap<String, Object>) t;
			String order = "",sort="";
			while (en.hasMoreElements()) {
				String nms = en.nextElement().toString();
				if(nms.endsWith("[]")){
					String[] as = request.getParameterValues(nms);
					if(as!=null&&as.length!=0&&as.toString()!="[]"){
						String mname = t.getClass().getSimpleName().toUpperCase();
						if(nms.toUpperCase().startsWith(mname)){
							nms=nms.substring(nms.toUpperCase().indexOf(mname)+1);
							map.put( nms,as);
						}
					}
				}else{
					String as = request.getParameter(nms);
					if ("".equals(as.trim())) {
						as = "nullStr";
					}
					if ("   ".equals(as)) {
						as = "nullStr";
					}
					if(!Common.isEmpty(as)){
						if ("nullStr".equals(as)) {
							as = null;
						}
						String mname = t.getClass().getSimpleName().toUpperCase();
						if(nms.toUpperCase().startsWith(mname)){
							nms=nms.substring(mname.length()+1);
							map.put( nms, as);
						}
						if(nms.toLowerCase().equals("column"))order = as;
						if(nms.toLowerCase().equals("sort"))sort = as;
					}
				}
			}
			if(!Common.isEmpty(order) && !Common.isEmpty(sort))
				map.put("orderby", " order by " + order + " " + sort);
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
		return  t;
	}
	
	/**
	 * 获取传递的所有参数,
	 * 再设置属性值
	 * 通过回传Map对象.
	 * <br/>
	 *<b>author：</b><br/> 
	 *<b>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsplijianning</b><br/> 
	 *<b>date：</b><br/> 
	 *<b>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp2015-04-01</b><br/> 
	 *<b>version：</b><br/> 
	 *<b>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp1.0v</b>
	 * @return Class<T>
	 * @throws Exception
	 */
	public <T> T findHasHMap(Class<T> clazz){
		HttpServletRequest request = ((ServletRequestAttributes)RequestContextHolder.getRequestAttributes()).getRequest();  
		Enumeration<String> en = request.getParameterNames();
		T t = null;
		try {
			t = clazz.newInstance();
			@SuppressWarnings("unchecked")
			FormMap<String, Object> map = (FormMap<String, Object>) t;
			while (en.hasMoreElements()) {
				String nms = en.nextElement().toString();
				if(!"_t".equals(nms)){
					if(nms.endsWith("[]")){
						String[] as = request.getParameterValues(nms);
						if(as!=null&&as.length!=0&&as.toString()!="[]"){
							map.put( nms,as);
						}
					}else{
						String as = request.getParameter(nms);
						if(!Common.isEmpty(as)){
							map.put( nms, as);
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return t;
	}
	
	
	//自定义数据通用方法
	private List<UDefinedDataFieldFormMap> getUDefinedDataFieldFormMap(String dataType) {
		UDefinedDataFormMap uDefinedDataFormMap = new UDefinedDataFormMap();
		uDefinedDataFormMap.put("data_type", dataType);
		uDefinedDataFormMap.put("site_id", ShiroSecurityHelper.getSiteId());
		List<UDefinedDataFormMap> listUDefinedDataFormMap = baseMapper.findByNames(uDefinedDataFormMap);

		List<UDefinedDataFieldFormMap> listUDefinedDataFieldFormMap = new ArrayList<UDefinedDataFieldFormMap>();
		if (ListUtils.isNotNull(listUDefinedDataFormMap)) {
			UDefinedDataFormMap uDefinedDataFormMap2 = listUDefinedDataFormMap.get(0);
			String id = uDefinedDataFormMap2.getStr("id");

			UDefinedDataFieldFormMap uDefinedDataFieldFormMap = new UDefinedDataFieldFormMap();
			uDefinedDataFieldFormMap.put("udefined_data_id", id);
			uDefinedDataFieldFormMap.put("site_id", ShiroSecurityHelper.getSiteId());
			uDefinedDataFieldFormMap.put("orderby", " order by data_level asc");
			listUDefinedDataFieldFormMap = baseMapper.findByNames(uDefinedDataFieldFormMap);
		}
		return listUDefinedDataFieldFormMap;
	}

	public List<UDefinedDataFieldFormMap> getUDefinedDataField(String dataType) {
		return getUDefinedDataFieldFormMap(dataType);
	}

	private List<UDefinedDataValueFormMap> getUDefinedDataValueFormMap(String dataDypeDetailId) {
		UDefinedDataValueFormMap uDefinedDataValueFormMap = new UDefinedDataValueFormMap();
		uDefinedDataValueFormMap.put("data_type_detail_id", dataDypeDetailId);
		uDefinedDataValueFormMap.put("site_id", ShiroSecurityHelper.getSiteId());
		List<UDefinedDataValueFormMap> listUDefinedDataValueFormMap = new ArrayList<UDefinedDataValueFormMap>();
		listUDefinedDataValueFormMap = baseMapper.findByNames(uDefinedDataValueFormMap);
		return listUDefinedDataValueFormMap;
	}

	public void getUDefinedDataValue(Model model, String dataDypeDetailId, String dataType,String modelAddArrtibute) {
		List<UDefinedDataFieldFormMap> listUDefinedDataFieldFormMap = getUDefinedDataFieldFormMap(dataType);

		if (ListUtils.isNotNull(listUDefinedDataFieldFormMap)) {
			List<UDefinedDataValueFormMap> listUDefinedDataValueFormMap = getUDefinedDataValueFormMap(dataDypeDetailId);
			if (ListUtils.isNotNull(listUDefinedDataFieldFormMap)) {
				for (int i = 0; i < listUDefinedDataFieldFormMap.size(); i++) {
					String fieldId = listUDefinedDataFieldFormMap.get(i).getStr("id");
					for (int j = 0; j < listUDefinedDataValueFormMap.size(); j++) {
						String udefinedDataValeFieldId = listUDefinedDataValueFormMap.get(j)
								.getStr("udefined_data_filed_id");
						if (udefinedDataValeFieldId.equals(fieldId)) {
							listUDefinedDataFieldFormMap.get(i).put("value",
									listUDefinedDataValueFormMap.get(j).getStr("value"));
						}
					}
				}
			}
			model.addAttribute("dataKeys", listUDefinedDataFieldFormMap);
		}
	}
}