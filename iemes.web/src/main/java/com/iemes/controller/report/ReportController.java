package com.iemes.controller.report;

import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.iemes.controller.index.BaseController;
import com.iemes.entity.FlowStepFormMap;
import com.iemes.entity.ShoporderFormMap;
import com.iemes.mapper.report.KanbanMapper;
import com.iemes.mapper.report.ReportMapper;
import com.iemes.service.CommonService;
import com.iemes.util.Common;
import com.iemes.entity.FormMap;
import com.iemes.util.ListUtils;
import com.iemes.util.ResponseHelp;
import com.iemes.util.ShiroSecurityHelper;

@Controller
@RequestMapping("/report/")
public class ReportController extends BaseController{

	@Inject
	private CommonService commonService;
	
	@Inject
	private ReportMapper reportMapper;
	
	@Inject
	private KanbanMapper kanbanMapper;
	
	private Logger log = Logger.getLogger(ReportController.class);
	
	/**
	 * 生产记录表（SFC列表）
	 * @return
	 */
	@RequestMapping("production_record_report")
	public String production_record_report(){
		return Common.BACKGROUND_PATH + "/report/production_record_report";
	}
	
	/**
	 * sfc装配信息报表（SFC）
	 * @return
	 */
	@RequestMapping("sfc_assembly_report")
	public String sfc_assembly_report(){
		return Common.BACKGROUND_PATH + "/report/sfc_assembly_report";
	}
	
	/**
	 * 生产记录表（工艺步骤列表）
	 * @return
	 */
	@RequestMapping("production_record_flowstep_report")
	public String production_record_flowstep_report(Model model, HttpServletRequest request) {
		FormMap<String, String> formMap = getFormMap(FormMap.class);
		try {
			String shoporderId = formMap.getStr("shoporder_id");
			String sfc = formMap.getStr("sfc");
			String siteId = ShiroSecurityHelper.getSiteId();
			formMap.put("sfc", sfc);
			formMap.put("site_id", siteId);
			formMap.put("shoporder_id", shoporderId);
			// 1.获取workflowid
			ShoporderFormMap shoporderFormMap = new ShoporderFormMap();
			shoporderFormMap.put("id", shoporderId);
			List<ShoporderFormMap> listShoporderFormMap = baseMapper.findByNames(shoporderFormMap);
			if (!ListUtils.isNotNull(listShoporderFormMap)) {
				log.error("未检索到sfc的工单信息");
				model.addAttribute("errorMessage", "未检索到sfc的工单信息");
				return Common.BACKGROUND_PATH + "/report/production_record_flowstep_report";
			}
			// 查工艺路线步驟
			shoporderFormMap = listShoporderFormMap.get(0);
			String workflowId = shoporderFormMap.getStr("process_workflow_id");
			// 2.获取工艺路线的首操作
			FlowStepFormMap flowStepFormMap = new FlowStepFormMap();
			flowStepFormMap.put("process_workflow_id", workflowId);
			flowStepFormMap.put("status", 1);
			List<FlowStepFormMap> listFlowStepFormMap = baseMapper.findByNames(flowStepFormMap);
			if (!ListUtils.isNotNull(listFlowStepFormMap)) {
				log.error("未检索到sfc的首操作信息");
				model.addAttribute("errorMessage", "未检索到工单的工艺路线信息");
				return Common.BACKGROUND_PATH + "/report/production_record_flowstep_report";
			}
			String firstOperationId = listFlowStepFormMap.get(0).getStr("operation_id");
			formMap.put("first_opearation_id", firstOperationId);
			formMap.put("work_flow_id", workflowId);

			List<Map<String, Object>> list = reportMapper.getProductionRecordFlowStepData(formMap);
			for (int i = 0; i < list.size(); i++) {
				Map<String, Object> map = list.get(i);
				map.put("step_level", i * 10);
				if (map.get("step_status") != null) {
					String status = map.get("step_status").toString();
					if (!StringUtils.isEmpty(status)) {
						map.put("step_status", commonService.getSfcStepStatusStr(Integer.parseInt(status)));
					}
				}
				map.put("assemble_info", "装配信息");
				map.put("nc_info", "不良信息");
			}
			
			/*
			 * List<Map<String,Object>> list = new ArrayList<Map<String,Object>>(); for (int
			 * i=0;i<4;i++) { Map<String,Object> map = new HashMap<String,Object>();
			 * map.put("step_level", i*10); map.put("process_workflow", "工艺路线"+i);
			 * map.put("operation_no", "操作"+i); map.put("sfc", "sfc"+i);
			 * map.put("main_item", "成品物料"+i); map.put("step_status", "排队中");
			 * map.put("workshop_no", "车间"+i); map.put("workline_no", "产线"+i);
			 * map.put("step_create_time", "2017-10-26 16:14:00");
			 * map.put("step_create_user", "创建人"+i); map.put("assemble_info", "装配信息");
			 * map.put("nc_info", "不良信息"); list.add(map); }
			 */
			
			
			List<Map<String,Object>> list2 = reportMapper.getProductionRecordSfcStepData(formMap);
			
			for (int i = 0; i < list2.size(); i++) {
				Map<String, Object> map = list2.get(i);
				if (map.get("step_status") != null) {
					String status = map.get("step_status").toString();
					if (!StringUtils.isEmpty(status)) {
						map.put("step_status", commonService.getSfcStepStatusStr(Integer.parseInt(status)));
					}
				}
				map.put("assemble_info", "装配信息");
				map.put("nc_info", "不良信息");
			}

			/*List<Map<String, Object>> list2 = new ArrayList<Map<String, Object>>();
			for (int i = 0; i < 10; i++) {
				Map<String, Object> map = new HashMap<String, Object>();
				map.put("sfc", i * 10);
				map.put("workshop_no", "车间" + i);
				map.put("workline_no", "产线" + i);
				map.put("work_resource_no", "资源" + i);
				map.put("operation_no", "操作" + i);
				map.put("step_status", "排队中");
				map.put("step_create_time", "2017-10-26 16:14:00");
				map.put("step_create_user", "创建人" + i);
				map.put("assemble_info", "装配信息");
				map.put("nc_info", "不良信息");
				list2.add(map);
			}*/
			model.addAttribute("list", list);
			model.addAttribute("list2", list2);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return Common.BACKGROUND_PATH + "/report/production_record_flowstep_report";
	}
	
	
	/**
	 * 生产记录表（sfc装配列表）
	 * @return
	 */
	@RequestMapping("production_record_assemblelist_report")
	public String production_record_assemblelist_report(Model model, HttpServletRequest request){
		FormMap<String, String> formMap = getFormMap(FormMap.class);
		try {
			String operationNo = formMap.getStr("operation_no");
			String sfc = formMap.getStr("sfc");
			String siteId = ShiroSecurityHelper.getSiteId();
			formMap.put("sfc", sfc);
			formMap.put("site_id", siteId);
			formMap.put("operation_no", operationNo);
			List<Map<String,Object>> list = reportMapper.getProductionRecordAssembleListData(formMap);
			/*List<Map<String,Object>> list = new ArrayList<Map<String,Object>>();
			for (int i=0;i<10;i++) {
				Map<String,Object> map = new HashMap<String,Object>();
				map.put("sfc", i*10+i);
				map.put("workshop_no", "车间"+i);
				map.put("workline_no", "产线"+i);
				map.put("operation_no", "操作"+i);
				map.put("work_resource_no", "资源"+i);
				map.put("assemble_item", "装配物料"+i);
				map.put("assemble_sfc", "装配物料SFC"+i);
				map.put("assemble_num", i);
				map.put("step_create_time", "2017-10-26 16:14:00");
				map.put("step_create_user", "创建人"+i);
				list.add(map);
			}*/
			model.addAttribute("list", list);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return Common.BACKGROUND_PATH + "/report/production_record_assemblelist_report";
	}
	
	/**
	 * sfc装配信息报表
	 * @return
	 */
	@RequestMapping("query_sfc_assembly_report")
	public String query_sfc_assembly_report(Model model, HttpServletRequest request){
		FormMap<String, String> formMap = getFormMap(FormMap.class);
		try {
			//String operationNo = formMap.getStr("operation_no");
			String sfc = request.getParameter("sfc");
			String siteId = ShiroSecurityHelper.getSiteId();
			formMap.put("sfc", sfc);
			formMap.put("site_id", siteId);
			//formMap.put("operation_no", operationNo);
			List<Map<String,Object>> list = reportMapper.getProductionRecordAssembleListData(formMap);
			model.addAttribute("list", list);
			model.addAttribute("sfc", sfc);
			model.addAttribute("successMessage", "检索成功");
		} catch (Exception e) {
			String errMessage = "检索失败" + e.getMessage();
			model.addAttribute("errorMessage", errMessage);
			e.printStackTrace();		
		}
		return Common.BACKGROUND_PATH + "/report/sfc_assembly_report";
	}
	
	
	/**
	 * 生产记录表（sfc不良列表）
	 * @return
	 */
	@RequestMapping("production_record_ncinfo_report")
	public String production_record_ncinfo_report(Model model, HttpServletRequest request){
		FormMap<String, String> formMap = getFormMap(FormMap.class);	
		try {
			String operationNo = formMap.getStr("operation_no");
			String sfc = formMap.getStr("sfc");
			String siteId = ShiroSecurityHelper.getSiteId();
			formMap.put("sfc", sfc);
			formMap.put("site_id", siteId);
			formMap.put("operation_no", operationNo);
			List<Map<String,Object>> list = reportMapper.getProductionRecordNcInfoData(formMap);
			/*List<Map<String,Object>> list = new ArrayList<Map<String,Object>>();
			for (int i=0;i<10;i++) {
				Map<String,Object> map = new HashMap<String,Object>();
				map.put("sfc", i*10+i);
				map.put("workshop_no", "车间"+i);
				map.put("workline_no", "产线"+i);
				map.put("operation_no", "操作"+i);
				map.put("work_resource_no", "资源"+i);
				map.put("nc_code_group", "不良代码组"+i);
				map.put("nc_code", "不良代码"+i);
				map.put("dispose_info", "已经处置");
				map.put("dispose_type", "报废");
				map.put("remark", "我爱报废，所以报废");
				list.add(map);
			}*/
			model.addAttribute("list", list);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return Common.BACKGROUND_PATH + "/report/production_record_ncinfo_report";
	}
	
	/**
	 * 生产进度表
	 * @return
	 */
	@RequestMapping("work_schedule_report")
	public String schedulerReportUi(){
		return Common.BACKGROUND_PATH + "/report/work_schedule_report";
	}

	/**
	 * 车间库存表
	 * @return
	 */
	@RequestMapping("workshop_inventory_report")
	public String Get_workshop_inventory_report_url(){
		return Common.BACKGROUND_PATH + "/report/workshop_inventory_report";
	}
	
	/**
	 * 车间物料状态看板（查询）
	 * @return
	 */
	@RequestMapping("workshop_item_consumption_kanban_query")
	public String Get_workshop_item_consumption_kanban_query_url(){
		return Common.BACKGROUND_PATH + "/report/workshop_item_consumption_kanban_query";
	}
	
	/**
	 * 车间物料状态看板（展示）
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping("workshop_item_consumption_kanban")
	public String Get_workshop_item_consumption_kanban_url(Model model, HttpServletRequest request){
		String workshopId = request.getParameter("workshop_id");
		model.addAttribute("workshop_id", workshopId);
		return Common.BACKGROUND_PATH + "/report/workshop_item_consumption_kanban";
	}

	/**
	 * 取报表数据方法（公共） 
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	@ResponseBody
	@RequestMapping("getReportData")
	public String getReportData(){
		FormMap<String, String> formMap = getFormMap(FormMap.class);
		formMap.put("site_id", ShiroSecurityHelper.getSiteId());
		String reportMethod = formMap.getStr("reportMethod");
		
		List<Map<String, Object>> list = null;
		
		try {
			switch (reportMethod) {
			case "getScheduleReportData":
				list = reportMapper.getScheduleReportData(formMap);
				break;
			case "getInventoryData":
				list = kanbanMapper.findKanBanData(formMap);
				break;
			case "getProductionRecordList":
				list = reportMapper.getProductionRecordListData(formMap);
				break;		
			default:
				break;
			}
		}catch(Exception e) {
			e.printStackTrace();
			log.error(e.getMessage(), e);
			return ResponseHelp.responseErrorText(e.getMessage());
		}
		return ResponseHelp.responseListToText(list);
	}

}
