package com.iemes.controller.workcenter_model;

import java.util.List;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.iemes.annotation.SystemLog;
import com.iemes.controller.index.BaseController;
import com.iemes.entity.WorkCenterFormMap;
import com.iemes.entity.WorkResourceFormMap;
import com.iemes.entity.WorkResourceTypeFormMap;
import com.iemes.exception.BusinessException;
import com.iemes.mapper.WorkcenterModel.WorkResourceMapper;
import com.iemes.service.workcenter_model.WorkResourceService;
import com.iemes.util.Common;
import com.iemes.util.DateUtils;
import com.iemes.util.ListUtils;
import com.iemes.util.ResponseHelp;
import com.iemes.util.ShiroSecurityHelper;

/**
 * 
 */
@Controller
@RequestMapping("/workcenter_model/work_resource/")
public class WorkResourceController extends BaseController {
	
	@Inject
	private WorkResourceMapper workResourceMapper;
	
	@Inject
	private WorkResourceService workResourceService;
	
	private Logger log = Logger.getLogger(this.getClass());
	private String workResourceMaintenanceUrl = Common.BACKGROUND_PATH + "/workcenter_model/work_resource/work_resource_maintenance";

	//页面跳转
	@RequestMapping("work_resource_maintenance")
	public String Get_work_resource_maintenance_Url(Model model,HttpServletRequest request) throws Exception {
		model.addAttribute("dataKeys", getUDefinedDataField("work_resource"));
		handlePageRes(model,request);
		return workResourceMaintenanceUrl;
	}
	
	/**
	 * 保存资源
	 * @return
	 */
	@ResponseBody
	@RequestMapping("saveWorkResource")
	@SystemLog(module="资源维护",methods="资源维护-保存资源")		//凡需要处理业务逻辑的.都需要记录操作日志
	public String saveWorkResource() {
		WorkResourceFormMap workResourceFormMap = getFormMap(WorkResourceFormMap.class);
		workResourceFormMap.put("create_time", DateUtils.getStringDateTime());
		workResourceFormMap.put("create_user", ShiroSecurityHelper.getCurrentUsername());
		workResourceFormMap.put("site_id", ShiroSecurityHelper.getSiteId());
		
		try {
			workResourceService.saveWorkResource(workResourceFormMap);	
		} catch (BusinessException e) {
			log.error(e.getMessage());
			return ResponseHelp.responseErrorText(e.getMessage());
		}
		return ResponseHelp.responseText();
	}
	

	/**
	 * 检索资源
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping("queryWorkResource")
	public String queryWorkResourceType(Model model, HttpServletRequest request) {
		String siteId = ShiroSecurityHelper.getSiteId();
		String workResourceNo = request.getParameter("work_resource_no");
		WorkResourceFormMap workResourceFormMap = new WorkResourceFormMap();
		workResourceFormMap.put("resource_no", workResourceNo);
		workResourceFormMap.put("site_id", siteId);

		List<WorkResourceFormMap> listWorkResourceFormMap = workResourceMapper.findByNames(workResourceFormMap);
		if (!ListUtils.isNotNull(listWorkResourceFormMap)) {
			String errMessage = "未检索到资源编号为：" + workResourceNo.toUpperCase() + "的信息";
			log.error(errMessage);
			model.addAttribute("dataKeys", getUDefinedDataField("work_resource"));
			model.addAttribute("errorMessage", errMessage);
			handlePageRes(model,request);
			return workResourceMaintenanceUrl;
		}
		workResourceFormMap = listWorkResourceFormMap.get(0);
		model.addAttribute("workResourceFormMap", workResourceFormMap);
		
		String worklineId = workResourceFormMap.getStr("workline_id");
		if (!StringUtils.isEmpty(worklineId)) {
			List<WorkCenterFormMap> listWorkCenterFormMap = baseMapper.findByAttribute("id", worklineId, WorkCenterFormMap.class);
			if (!ListUtils.isNotNull(listWorkCenterFormMap)) {
				String errMessage = "未检索到相关的所属工作中心信息，请确认对应的工作中心信息是否存在或被删除";
				log.error(errMessage);
				model.addAttribute("dataKeys", getUDefinedDataField("work_resource"));
				model.addAttribute("errorMessage", errMessage);
				handlePageRes(model,request);
				return workResourceMaintenanceUrl;
			}
			model.addAttribute("workCenterFormMap", listWorkCenterFormMap.get(0));
		}
		
		
		getUDefinedDataValue(model,workResourceFormMap.getStr("id"),"work_resource","dataKeys");
		
		model.addAttribute("successMessage", "检索成功");
		handlePageRes(model,request);

		return workResourceMaintenanceUrl;
	}
	
	/**
	 * 删除资源
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping("delWorkResource")
	public String delWorkResource(Model model, HttpServletRequest request) {
		String workResourceId = request.getParameter("work_resource_id");
		try {
			if (StringUtils.isEmpty(workResourceId)) {
				throw new BusinessException("操作失败，参数不能为空！");
			}
			workResourceService.delWorkResource(workResourceId);
		} catch (BusinessException e) {
			String errMessage = e.getMessage();
			log.error(errMessage);
			model.addAttribute("dataKeys", getUDefinedDataField("work_resource"));
			model.addAttribute("errorMessage", errMessage);
			handlePageRes(model,request);
			return workResourceMaintenanceUrl;
		}
		model.addAttribute("dataKeys", getUDefinedDataField("work_resource"));
		model.addAttribute("successMessage", "删除资源成功");
		handlePageRes(model,request);
		return workResourceMaintenanceUrl;
	}
	
	// 获取资源类型列表
	@ResponseBody
	@RequestMapping("work_resource_type_list")
	public List<WorkResourceTypeFormMap> work_resource_type_list(Model model) throws Exception {
		WorkResourceTypeFormMap workResourceTypeFormMap = getFormMap(WorkResourceTypeFormMap.class);
		workResourceTypeFormMap.put("site_id", ShiroSecurityHelper.getSiteId());
		workResourceTypeFormMap.put("orderby", " order by create_time asc ");
		List<WorkResourceTypeFormMap> mps = workResourceMapper.findByNames(workResourceTypeFormMap);	
		return mps;
	}

}