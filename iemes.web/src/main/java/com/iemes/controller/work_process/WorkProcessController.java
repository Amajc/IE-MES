package com.iemes.controller.work_process;


import java.io.IOException;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.session.Session;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.iemes.annotation.SystemLog;
import com.iemes.controller.index.BaseController;
import com.iemes.entity.SiteFormMap;
import com.iemes.entity.UserFormMap;
import com.iemes.entity.UserGroupsFormMap;
import com.iemes.exception.SystemException;
import com.iemes.mapper.UserMapper;
import com.iemes.mapper.site.SiteMapper;
import com.iemes.plugin.PageView;
import com.iemes.util.Common;
import com.iemes.util.JsonUtils;
import com.iemes.util.POIUtils;
import com.iemes.util.PasswordHelper;

/**
 * 
 * @author mds
 * @Email: mds
 * @version 2.0v
 */
@Controller
@RequestMapping("/work_process/")
public class WorkProcessController extends BaseController {
	
	//页面跳转
	@RequestMapping("barcode_printing")
	public String Get_barcode_printing_Url(Model model) throws Exception {
		model.addAttribute("res", findByRes());
		return Common.BACKGROUND_PATH + "/work_process/barcode_printing/barcode_printing";
	}
	
	@RequestMapping("pod_function_maintenance")
	public String Get_pod_function_maintenance_Url(Model model) throws Exception {
		model.addAttribute("res", findByRes());
		return Common.BACKGROUND_PATH + "/work_process/pod_function/pod_function_maintenance";
	}
	
	@RequestMapping("pod_button_maintenance")
	public String Get_pod_button_maintenance_Url(Model model) throws Exception {
		model.addAttribute("res", findByRes());
		return Common.BACKGROUND_PATH + "/work_process/pod_button/pod_button_maintenance";
	}
	
	@RequestMapping("pod_panel_maintenance")
	public String Get_pod_panel_maintenance_Url(Model model) throws Exception {
		model.addAttribute("res", findByRes());
		return Common.BACKGROUND_PATH + "/work_process/pod_panel/pod_panel_maintenance";
	}

	@RequestMapping("pod_opeartion")
	public String Get_pod_opeartion_Url(Model model) throws Exception {
		model.addAttribute("res", findByRes());
		return Common.BACKGROUND_PATH + "/work_process/pod_opeartion/pod_opeartion";
	}
}