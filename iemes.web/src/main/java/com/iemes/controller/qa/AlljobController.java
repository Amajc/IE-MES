package com.iemes.controller.qa;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.iemes.controller.index.BaseController;
import com.iemes.util.Common;


@Controller
@RequestMapping("/qa/alljob/")
public class AlljobController extends BaseController {
	
	private String backUrl = Common.BACKGROUND_PATH + "/qa/alljob/alljob";
	
	@RequestMapping("list")
	public String allJobList() {
		return backUrl;
	}

}
