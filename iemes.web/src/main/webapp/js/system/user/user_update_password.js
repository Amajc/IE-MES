$(document).ready(function() {
	
	if (!stringIsNull($('#errorMessage').val())) {
		showErrorNoticeMessage($('#errorMessage').val());
	}
	
	if (!stringIsNull($('#successMessage').val())) {
		showSuccessNoticeMessage($('#successMessage').val());
	}
	
	$("form").submit(function() {
		var data = $("form").serialize();
		$.post(rootPath + "/system/user/updatePass.shtml", data, function(e, status, xhr){
			var data= JSON.parse(e);
			if (data.status) {
				showSuccessNoticeMessage("密码修改成功");
			}else {
				showErrorNoticeMessage(data.message);
			}
		},"json")
		return false;
	})
	
	
})