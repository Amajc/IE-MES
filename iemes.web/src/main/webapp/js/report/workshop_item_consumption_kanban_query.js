$(document).ready(function() {
	//检索
	$('#btnQuery').click(function (){
		var workshop_id = $("#input_workshop_id").val();
		if(workshop_id==null || workshop_id==""){
			showErrorNoticeMessage('车间不能为空');
			return;
		}
		var kanbanUrl = rootPath + "/report/workshop_item_consumption_kanban.shtml?workshop_id="+workshop_id;
		window.open(kanbanUrl);
	})	
	
	//清除
	$('#btnClear').click(function (){
		var tb = $(".index_centent");
		tb.empty();
		tb.load(rootPath + "/report/workshop_item_consumption_kanban_query.shtml");
	})
})



