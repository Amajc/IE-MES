//关闭通知信息
function closeNoticeMessage() {
	var $noticeMsgDiv = $("#pageNoticeInformation");
	var $noticeMsgLabel = $("#pageNoticeInformation").find("label");
	//$noticeMsgLabel.text("");
	$noticeMsgDiv.fadeOut("slow");	
	$.MsgLoading.UnLoading();
}
//显示失败通知信息
function showErrorNoticeMessage(message) {
	var $noticeMsgDiv = $("#pageNoticeInformation");
	var $noticeMsgLabel = $("#pageNoticeInformation").find("label");

	$noticeMsgDiv.css("color","red");
	$noticeMsgLabel.text(message);
	$.MsgLoading.UnLoading();
	$noticeMsgDiv.fadeIn("slow");
}

//显示成功通知信息
function showSuccessNoticeMessage(message) {
	var $noticeMsgDiv = $("#pageNoticeInformation");
	var $noticeMsgLabel = $("#pageNoticeInformation").find("label");

	$noticeMsgDiv.css("color","green");
	$noticeMsgLabel.text(message);
	$.MsgLoading.UnLoading();
	$noticeMsgDiv.fadeIn("slow");
}

//判断字符串是否为空
function stringIsNull(data){ 
	return (data == "" || data == undefined || data == null) ? true : false; 
}


//单条选中，从左向右移
function selected() {
	var selOpt = $("#groupsForSelect option:selected");

	selOpt.remove();
	var selObj = $("#selectGroups");
	selObj.append(selOpt);

	var selOpt = $("#selectGroups")[0];
	ids = "";
	for (var i = 0; i < selOpt.length; i++) {
		ids += (selOpt[i].value  + ",");
	}

	if (ids != "") {
		ids = ids.substring(0, ids.length - 1);
	}
	$('#txtGroupsSelect').val(ids);
}

//全部从左向右移
function selectedAll() {
	var selOpt = $("#groupsForSelect option");

	selOpt.remove();
	var selObj = $("#selectGroups");
	selObj.append(selOpt);

	var selOpt = $("#selectGroups")[0];
	ids = "";
	for (var i = 0; i < selOpt.length; i++) {
		ids += (selOpt[i].value  + ",");
	}

	if (ids != "") {
		ids = ids.substring(0, ids.length - 1);
	}
	$('#txtGroupsSelect').val(ids);
}

//单条选中，从右向左移
function unselected() {
	var selOpt = $("#selectGroups option:selected");
	selOpt.remove();
	var selObj = $("#groupsForSelect");
	selObj.append(selOpt);

	var selOpt = $("#selectGroups")[0];
	ids = "";
	for (var i = 0; i < selOpt.length; i++) {
		ids += (selOpt[i].value + ",");
	}
	
	if (ids != "") {
		ids = ids.substring(0, ids.length - 1);
	}
	$('#txtGroupsSelect').val(ids);
}

//全部从右向左移
function unselectedAll() {
	var selOpt = $("#selectGroups option");
	selOpt.remove();
	var selObj = $("#groupsForSelect");
	selObj.append(selOpt);

	$('#txtGroupsSelect').val("");
}
