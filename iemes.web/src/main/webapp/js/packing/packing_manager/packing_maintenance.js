$(document).ready(function() {

	if (!stringIsNull($('#errorMessage').val())) {
		showErrorNoticeMessage($('#errorMessage').val());
	}

	if (!stringIsNull($('#successMessage').val())) {
		showSuccessNoticeMessage($('#successMessage').val());
	}
	
	var generateCodeFun = function (){
		var container_type_id = $('#tbx_container_type_id').val();
		if (container_type_id=='') {
			showErrorNoticeMessage("提示信息：请先检索容器类型后再进行该操作！！！");
			return;
		}
		var data = $("form").serialize();
		$.post(rootPath + "/packing/getContainerId.shtml", data, function(e, status, xhr){
			var data= JSON.parse(e);
			if (data.status) {
				$('#input_container_id').val(data.message);
				$('#container_status').val("1");
				$.MTable.delAll("containerSfcList");
			}else {
				showErrorNoticeMessage(data.message);
			}
		},"json")
	}
	
	var delRow = function (obj){
		debugger;
		var i = obj.parentNode.parentNode.rowIndex;
		
		var wrappage = $(obj.parentNode.parentNode).find('td').eq(0).text();
		var container_type_id = $('#tbx_container_type_id').val();	//容器类型ID
		var container_id = $('#input_container_id').val();			//容器条码
		var container_status = $('#container_status').val();		//容器状态
		
		if(container_status == "-1"){
			showErrorNoticeMessage("提示信息, 容器【"+container_id+"】是关闭状态，不能进行该操作！！！");
			return;
		}
		
		var data = "containerSFCFormMap.container_id="+container_id+"&containerSFCFormMap.container_type_id="+container_type_id+"&containerSFCFormMap.wrappage="+wrappage
		
		$.post(rootPath + "/packing/unPacking.shtml", data, function(e, status, xhr){
			var data= JSON.parse(e);
			if (data.status) {
				showSuccessNoticeMessage(wrappage+"已成功解包");
				if (i > 0) {
					containerSfcList.deleteRow(i);
				}
			}else {
				showErrorNoticeMessage(data.message);
			}
		},"json")
	}
	
	//移除
	$('.bt_delete_row').click(function (){
		delRow(this);
	})
	
	//生成新的容器条码
	$('#generateCode').click(function (){
		generateCodeFun();
	})

	//检索
	$('#btnQuery').click(function (){
		var container_type_no = $('#tbx_container_type_no').val();
		var container_id = $('#input_container_id').val();
		
		var tb = $(".index_centent");
		tb.empty();
		tb.load(rootPath + "/packing/queryPackContainer.shtml?container_type_no="+container_type_no+"&container_id="+container_id);
	})

	//清除
	$('#btnClear').click(function (){
		var tb = $(".index_centent");
		tb.empty();
		tb.load(rootPath + "/packing/packing_manager.shtml");
	})

	//解包btnOpen
	$('#btnOpen').click(function (){
		var container_id = $('#input_container_id').val();
		if(container_id == ""){
			showErrorNoticeMessage("提示信息, 容器条码为空，无法解包容器！！！");
			return;
		}
		var data = "containerFormMap.container_id="+container_id;
		$.post(rootPath + "/packing/openContainer.shtml", data, function(e, status, xhr){
			var data= JSON.parse(e);
			if (data.status) {
				$('#container_status').val("1");
				showSuccessNoticeMessage("提示信息, 容器【"+container_id+"】解包成功！！！");
			}else {
				showErrorNoticeMessage(data.message);
			}
		},"json")
	})
	
	$('#tbx_container_type_no').change(function (){
		$('#input_container_id').val("");
		$('#Container_count').text("");
		$('#max_num').text("");
	})

	//打包btnClose
	$('#btnClose').click(function (){
		var container_id = $('#input_container_id').val();
		if(container_id == ""){
			showErrorNoticeMessage("提示信息, 容器条码为空，无法解包容器！！！");
			return;
		}
		var data = "containerFormMap.container_id="+container_id;
		$.post(rootPath + "/packing/closeContainer.shtml", data, function(e, status, xhr){
			var data= JSON.parse(e);
			if (data.status) {
				$('#container_status').val("-1");
				showSuccessNoticeMessage("提示信息, 容器【"+container_id+"】打包成功！！！");
			}else {
				showErrorNoticeMessage(data.message);
			}
		},"json")
	})

	//添加SFC btnAddSFC
	$('#btnAddSFC').click(function (){
		debugger;
		
		var container_type_id = $('#tbx_container_type_id').val();	//容器类型ID
		var container_id = $('#input_container_id').val();			//容器条码
		var container_status = $('#container_status').val();		//容器状态
		var wrappage = $('#tbx_sfc_no').val();						//包装的货物	
		var max_num = $('#max_num').text();							//容器最大数量
		var Container_count = $('#Container_count').text();			//容器当前已装数量
		var packingNum = $('#containerSfcList tr').length-1;		//容器已装数量
		
		if (Container_count==undefined || Container_count==null || Container_count=='') {
			showErrorNoticeMessage("提示信息：请检索容器类型后再进行此操作！！！");
			return;
		}
		
		if(container_status == "-1"){
			showErrorNoticeMessage("提示信息, 容器【"+container_id+"】是关闭状态，不能添加！！！");
			return;
		}
		
		if (container_type_id=="") {
			showErrorNoticeMessage("提示信息,  请检索容器类型后再进行此操作！！！");
			return;
		}
		
		if (container_id=="") {
			showErrorNoticeMessage("提示信息,  请生成容器条码后再进行此操作！！！");
			return;
		}
		
		if ($('#max_num').text()==($('#containerSfcList tr').length-1)) {
			showErrorNoticeMessage("提示信息, 已达箱子最大容量，请更换容器再进行包装！！！");
			return;
		}
		
		var tbx_sfc_no = $('#tbx_sfc_no').val();
		if(tbx_sfc_no == ""){
			showErrorNoticeMessage("提示信息, 车间作业控制，不能为空！！！");
			return;
		}else{
			//执行添加按钮前 校验是否已经添加过了重复的sfc
			var udata = $.MTable.getTableData("containerSfcList");
			var arr = [];
			for(var i= 0; i< udata.length;i++){
				var sfc = udata[i].value.value;
				arr.push(sfc);
			}
			//判断是否存在数组内  -1 不存在 
			var dat = $.inArray(tbx_sfc_no, arr);
		
			if(dat == -1){
				//插入到数据库
				var data = "containerSFCFormMap.container_id="+container_id+"&containerSFCFormMap.container_type_id="+
							container_type_id+"&containerSFCFormMap.wrappage="+wrappage+"&containerSFCFormMap.max_num="+max_num+"&containerSFCFormMap.packingNum="+packingNum
				
				$.post(rootPath + "/packing/packing.shtml", data, function(e, status, xhr){
					var data= JSON.parse(e);
					if (data.status) {
						if (data.message!="") {
							showSuccessNoticeMessage(data.message);
							$('#container_status').val("-1");
						}else {
							showSuccessNoticeMessage(wrappage+"包装成功");
						}
						
						var newRow = "<tr>" +
								"<td data-value="+tbx_sfc_no+" >"+tbx_sfc_no+"</td>" +
								"<td><a href='#' class='bt_delete_row'>删除</a></td>"
							    "</tr>";
						$.MTable.addRow("containerSfcList",newRow);
						
						$('.bt_delete_row').bind("click", function (){
							delRow(this);
						});
						
						$('#tbx_sfc_no').val("");
					}else {
						showErrorNoticeMessage(data.message);
						return;
					}
				},"json")
			}else{
				showErrorNoticeMessage("提示信息, 容器【"+container_id+"】不能重复添加【"+wrappage+"】");
				$('#tbx_sfc_no').val("");
			}
		}
	})
	
})

function chooseContainerType(row){
	if (row.container_level=="box") {
		$('#packingType').text("车间作业控制:");
		$('#packingType').data("value", "box");
		
		$('#tbx_sfc_no').attr("dataValue", "sfc");
		$('#tbx_sfc_no').attr("viewTitle", "车间作业控制");
		$('#tbx_sfc_no').attr("data-url", "/popup/getFinishAndUnPackingSfc.shtml");
	}else if (row.container_level=="carton"){
		$('#packingType').text("礼盒:");
		$('#packingType').data("value", "carton");
		
		$('#tbx_sfc_no').attr("dataValue", "container_id");
		$('#tbx_sfc_no').attr("viewTitle", "礼盒");
		$('#tbx_sfc_no').attr("data-url", "/popup/getUnPackingBox.shtml");
	}else if (row.container_level=="pallet"){
		$('#packingType').text("中箱:");
		$('#packingType').data("value", "pallet");
		
		$('#tbx_sfc_no').attr("dataValue", "container_id");
		$('#tbx_sfc_no').attr("viewTitle", "中箱");
		$('#tbx_sfc_no').attr("data-url", "/popup/getUnPackingCarton.shtml");
	}
}