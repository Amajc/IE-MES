var popup_inp;

function setInputValue(row){
	var elements = [];
	var dataValue = $(popup_inp).attr("dataValue");				//回填的字段名
	var relationId = $(popup_inp).attr("relationId");			//关联的控件
	var callBackFun = $(popup_inp).attr("callBackFun");			//回调函数
	if (callBackFun!=undefined && callBackFun!=null && callBackFun!="") {
		window[callBackFun](row);
	}
	var id = $(popup_inp).attr("id");
	
	var newElment = {};
	newElment.dataValue = dataValue;
	newElment.id = id;
	elements.push(newElment);
	
	//控件自身ID
	while (relationId!=undefined && relationId!=null &&
			dataValue!=undefined && dataValue!=null) {
		
		id = $('#'+relationId).attr("id");
		dataValue = $('#'+relationId).attr("dataValue");
		relationId = $('#'+relationId).attr("relationId");
		
		var exist = false;
		for (var i=0;i<elements.length;i++) {
			var element = elements[i];
			if (element.id==id) {
				exist = true;
				break;
			}
		}
		
		if (exist) break; 
		
		var newElment = {};
		newElment.dataValue = dataValue;
		newElment.id = id;
		elements.push(newElment);
	}
	
	for (var i=0;i<elements.length;i++) {
		var element = elements[i];
		$('#'+element.id).val(eval("row."+element.dataValue));
	}
	
	//绑定清空事件
	$(popup_inp).bind("keydown", function (e){
		for (var i=0;i<elements.length;i++) {
			var element = elements[i];
			if (element.id!=this.id) {
				$('#'+element.id).val("   ");
			}
		}
	});
	
	/*var viewV = $(popup_inp).attr("viewValue");				//显示的值的key
	var modelV = $(popup_inp).attr("modelValue");			//提交的值的key
	var viewValue = eval("row."+viewV);						//显示的值
	var modelValue = eval("row."+modelV);					//提交的值
	if ($(popup_inp).attr("modelIn")!=undefined) {			
		var modelIn = $('#'+$(popup_inp).attr("modelIn"));	//显示与提交的值不一样
		$(modelIn).val(modelValue);
		console.log($(modelIn).val());
	}
	$(popup_inp).val(viewValue);*/							//如何没有modelIn属性，则表示显示的值就是要提交的值
}

function operationBrowse(obj){
	popup_inp =$('#'+$(obj).attr("textFieldId"));		//显示的inp
	var inputValue = $(popup_inp).val();				//已经输入的值
	inputValue = escape(inputValue);
	var viewTitle = $(popup_inp).attr("viewTitle");		//弹出页面显示的title
	var filterIds = $(popup_inp).attr("filterId");		//其它过滤值
	var beforFun = $(popup_inp).attr("beforFun");		//回调函数
	if (beforFun!=undefined && beforFun!= null && beforFun!="") {
		var rs = window[beforFun]();
		if (!rs) {
			return;
		}
	}
	var filterMap = "";
	if (filterIds!=undefined && filterIds!=null) {
		var filters = filterIds.split(",");
		for (var i=0;i<filters.length;i++) {
			var filter = filters[i];
			if ($('#'+filter)!=undefined && $('#'+filter)!=null && $('#'+filter).length>0) {
				if ($('#'+filter).val()!="") {
					var v = $('#'+filter).val();
					filterMap = filterMap + filter + "=" + v;
					if (i!=filters.length-1) {
						filterMap = filterMap + "&";
					}
				}else {
					var msg = $('#'+filter).attr("validate_errormsg");
					$.MsgBox.Alert("系统提示", msg);
					return;
				}
			}else {
				$.MsgBox.Alert("系统提示", "控件："+filters[i]+"不存在");
				return;
			}
		}
	}
	var data_url = rootPath + $(popup_inp).attr("data-url")+"?inputValue="+inputValue+"&viewTitle="+viewTitle + "&"+filterMap;			//获取的地址
	var openwin = function (iWidth, iHeight){
		var itop = (window.screen.availHeight - 30 - iWidth) / 2;
		var ileft = (window.screen.availWidth - 10 - iHeight) / 2;
		window.open(data_url,'MDS IE-MES',"height="+iHeight+", width="+iWidth+", top="+itop+", left="+ileft+
				",toolbar=no, menubar=no, scrollbars=no, resizable=no, location=no, status=no");
	}
	openwin(720,600);
}

/*$(function (){
	$('.operationBrowse').click(function (e){
		popup_inp =$('#'+$(this).attr("textFieldId"));		//显示的inp
		var inputValue = $(popup_inp).val();				//已经输入的值
		var viewTitle = $(popup_inp).attr("viewTitle");		//弹出页面显示的title
		var data_url = rootPath + $(popup_inp).attr("data-url")+"?inputValue="+inputValue+"&viewTitle="+viewTitle;			//获取的地址
		var openwin = function (iWidth, iHeight){
			var itop = (window.screen.availHeight - 30 - iWidth) / 2;
			var ileft = (window.screen.availWidth - 10 - iHeight) / 2;
			window.open(data_url,'MDS IE-MES',"height="+iHeight+", width="+iWidth+", top="+itop+", left="+ileft+
					",toolbar=no, menubar=no, scrollbars=no, resizable=no, location=no, status=no");
		}
		openwin(720,600);
	});
})*/