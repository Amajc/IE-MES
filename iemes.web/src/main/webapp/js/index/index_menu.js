$(document).ready(function() {
	//$(".menu ul").css("display", "none");
	var menu_Level2ShowIconPath = "square inside url(\'icon/tree_triangle2.png\')";
	var menu_Level2HideIconPath = "square inside url(\'icon/tree_triangle.png\')";
	var menu_Level2ClickFlag = false;
	
	$(".menu_li").on("click", function() {
		if(menu_Level2ClickFlag == false){
			$(this).children("a").next().toggle();	
			var isChildrenOpen = $(this).attr("isChildrenOpen");
			if(isChildrenOpen == "true"){
				$(this).css("list-style",menu_Level2HideIconPath);
				$(this).attr("isChildrenOpen","false"); 
			}else if(isChildrenOpen == "false"){
				$(this).css("list-style",menu_Level2ShowIconPath);
				$(this).attr("isChildrenOpen","true"); 
			}				
		}
		else{
			menu_Level2ClickFlag = false;
		}
	});
	$(".menu_li2").on("click", function() {
		menu_Level2ClickFlag = true;
	});
	
	$('.menu_li_children').click(function (e){
		var name = this.innerText;
		var url = rootPath + $(this).attr("data-url");		
		var tb = $(".index_centent");
	    var sub_header = $(".index_subheader");
	    
		tb.empty();
		tb.load(url);	
		sub_header.find("span").text(name);
		
		sub_header.find("#div_builder_page_toolbar").empty();
		$('.iedex_subheader_sop').show();
		
		//sop
		var resourcesId = $(this).attr("data-resources-id");	
		if(resourcesId!=null){
			resourcesId = $.trim(resourcesId);	
		}else{
			return;
		}
		$('.iedex_subheader_sop').attr("resources_id",resourcesId)
		$('.iedex_subheader_sop').attr("resources_name",name)
	});	
	
	$('.iedex_subheader_sop').click(function (e){
		var resourcesId = $(this).attr("resources_id");	
		var resourcesName = $(this).attr("resources_name");	
		if(resourcesId!=null){
			resourcesId = $.trim(resourcesId);
		}else{
			return;
		}
		if(resourcesId!=""){
			resourcesId = $.trim(resourcesId);
		}else{
			$.MsgBox.Alert("系统消息","此项无SOP");
			return;
		}
		if(resourcesName!=null){
			resourcesName = $.trim(resourcesName);
		}else{
			resourcesName = "";
		}
		var sopViewerUrl = rootPath + "/work_scheduling/sop_manager/querySopViewer.shtml?resources_id="+resourcesId +"&resources_text="+resourcesName;
		window.open(sopViewerUrl);
	});	

});
