$(document).ready(function() {
	
	//保存
	$("#repair_form").submit(function() {
		var data = $("form").serialize();
		
		$.post(rootPath + "/work_process/work_pod_panel/podRepair.shtml", data, function(e, status, xhr){
			var data= JSON.parse(e);
			if (data.status) {
				showSuccessNoticeMessage("SFC维修成功，已成功进入生产");
			}else {
				showErrorNoticeMessage(data.message);
			}
		},"json")
		return false;
	});
});
