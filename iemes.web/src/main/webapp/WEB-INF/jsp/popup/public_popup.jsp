<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<%@include file="/js/common/common.jspf"%>
<script type="text/javascript" src="${ctx}/js/common/mtable.js"></script>
<script type="text/javascript" src="${ctx}/js/popup/public_popup.js"></script>
<title>MDS IE-MES</title>
</head>
<body class="openPopup">
	<div class="openPopup_title">浏览&nbsp;&nbsp;${mTable.tableName}</div>
	<form class="mds_tab_form">
		<table class="mtable" id="public_popup_table">
			<tr></tr>
			<tr>
				<c:forEach var="cols" items="${mTable.cols}">
					<c:if test="${cols.hide==-1 }">
						<th class="mtable_head_hide" data-value="${cols.colKey}">${cols.colName}</th>
					</c:if>
					<c:if test="${cols.hide==1 }">
						<th data-value="${cols.colKey}">${cols.colName}</th>
					</c:if>
				</c:forEach>
			</tr>
			<c:forEach var="data" items="${mTable.tableData}">
				<tr>
					<c:forEach var="cols" items="${mTable.cols}">
						<c:set var='key' value="${cols.colKey}" scope="page"/>
						<c:if test="${cols.hide==-1 }">
							<td class="mtable_head_hide" >${data[key]}</td>
						</c:if>
						<c:if test="${cols.hide==1 }">
							<td>${data[key]}</td>
						</c:if>
					</c:forEach>
				</tr>
			</c:forEach>
		</table>
	</form>
</body>
</html>