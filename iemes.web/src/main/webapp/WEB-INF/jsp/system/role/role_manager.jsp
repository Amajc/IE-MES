<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>	
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>角色管理</title>

<link rel="stylesheet" href="css/system/role/role_manager.css" type="text/css" />

<script type="text/javascript" src="js/common/form_validate.js"></script>
<script type="text/javascript" src="js/common/jtab.js"></script>
<script type="text/javascript" src="js/common/mtable.js"></script>
<script type="text/javascript" src="js/system/role/role_manager.js"></script>

</head>
<body>
<form class="mds_tab_form">
	<div id="commanButtonsDiv" class=iemes_style_commanButtonsDiv>
			<c:forEach var="resRow" items="${res}">
				<c:if test="${resRow.type==2 && resRow.is_hide!=1}">
					<c:choose>
						<c:when test="${resRow.res_url=='btnSave'}">
							<input type="submit" id="btnSave" value="${resRow.res_name}">
						</c:when>
						<c:otherwise>
							<input type="button" id="${resRow.res_url}"
								value="${resRow.res_name}">
						</c:otherwise>
					</c:choose>
				</c:if>
			</c:forEach>
	</div>
	
	<div class="hidden">
		<input type="hidden" value="${page_res_id}" id="page_res_id">
		<input type="hidden" name="roleFormMap.id" value="${role.id}" id="role_id">
		<input type="hidden" value="${errorMessage}" id="errorMessage">
		<input type="hidden" value="${successMessage}" id="successMessage">
	</div>

	<div id="pageNoticeInformation" class=iemes_style_NoticeDiv>
		 <label class="mds_message_label"></label>
		 <span class="iemes_style_NoticeButton"> <input type="button" submit="N" onclick="closeNoticeMessage();" value="关闭"> </span>
	</div>

	<div class="inputForm_query">
		<div class="formField_query">
			<label class="must">站点:</label> <span class="formText_query">${site}</span>
		</div>
		<div class="formField_query">
			<label class="must">角色编号:</label> 
			<input type="text"
				dataValue="role_key" 
				viewTitle="角色"
				data-url="/popup/queryAllRole.shtml" 
				class="formText_query majuscule" id="tbx_role_id" name="roleFormMap.role_key" value="${role.role_key }"
				validate_allowedempty="N" validate_errormsg="角色编号不能为空！" />
			<input type="button" submit="N" value="检索"  onclick="operationBrowse(this)"  textFieldId="tbx_role_id">
		</div>
	</div>


	<!--  Tab -->
	<ul id="tabs" class="tabs_li_3tabs">
		<li><a href="#" tabid="tab1">基础信息</a></li>
		<li><a href="#" tabid="tab2">用户绑定</a></li>
		<li><a href="#" tabid="tab3">权限绑定</a></li>
	</ul>
	<div id="content">
		<div id="tab1">
			<div class="inputForm_content">
			<div class="formField_content">
					<label class="must">角色名:</label> <input type="text" name="roleFormMap.role_name" value="${role.role_name }"
					validate_allowedempty="N" validate_errormsg="角色名不能为空！"
						class="formText_content" id="tbx_role_des" />
				</div>
				<div class="formField_content">
					<label>描述:</label> <input type="text" name="roleFormMap.description" value="${role.description }"
						class="formText_content" id="tbx_role_des" />
				</div>
				<div class="formField_content">
					<label class="must">状态:</label> 
					<select class="formText_content"  id="status" name="roleFormMap.status">
					<c:if test="${role.status==0}">
						<option value="0" selected="selected">可用</option>
						<option value="1">禁用</option>
					</c:if>
                    <c:if test="${role.status==1}">
						<option value="0">可用</option>
						<option value="1" selected="selected">禁用</option>
					</c:if>
					<c:if test="${role==null}">
						<option value="0" selected="selected">可用</option>
						<option value="1">禁用</option>
					</c:if>
                    </select>
				</div>
				<div class="formField_content">
					<label>创建人:</label> <span class="formText_content">${role.create_user }</span>
				</div>
				<div class="formField_content">
					<label>创建时间:</label> <span class="formText_content">${role.create_time }</span>
				</div>	
			</div>
		</div>
		<div id="tab2">
			<div class="inputForm_content">
					<table name="groups_tweenbox" class="groups_tweenTable">
						<tbody>
							<tr>
								<td>已绑定用户</td>
								<td></td>
								<td>可绑定用户</td>
							</tr>
							<tr>
								<td><select id="selectGroups" multiple="multiple"
									name="roleFormMap.userSelectGroups">
										<c:forEach var="user" items="${bindUserList}">
											<option value="${user.id }">${user.user_name}</option>
										</c:forEach>
								</select></td>
								<td>
									<div class="groups_tweenTable_centerDiv">
										<button onclick="selectedAll()" type="button" submit="N" title="全选">&lt;&lt;</button>
									</div>
									<div class="groups_tweenTable_centerDiv">
										<button onclick="selected()" type="button" submit="N" title="选择">&lt;</button>
									</div>
									<div class="groups_tweenTable_centerDiv">
										<button onclick="unselected()" type="button" submit="N" title="取消">&gt;</button>
									</div>
									<div class="groups_tweenTable_centerDiv">
										<button onclick="unselectedAll()" type="button"  submit="N" title="全取消">&gt;&gt;</button>
									</div>
								</td>
								<td>
								<select id="groupsForSelect" multiple="multiple">
									<c:forEach var="user" items="${noBindUserList}">
										<option value="${user.id }">${user.user_name}</option>
									</c:forEach>
								</select></td>
							</tr>
						</tbody>
					</table>

				</div>
		</div>
		<div id="tab3">
			<table class="mtable" id="role_resource_table">
				<tr>
					<th value="check" class="role_mtable_col1">选择</th>
					<th value="res_id" class="role_mtable_col2">权限及资源</th>
				</tr>
				<tr>
					<td class="role_mtable_col1"><input id="checkAll" type="checkbox"></td>
					<td class="role_mtable_col2 mtable_role_level_1" value="0">所有权限</td>
				</tr>
				<c:forEach var="resource" items="${resourceList}">
					<tr>
					
						<c:if test="${resource.is_check==1}">
							<td class="role_mtable_col1"><input checked="checked" type="checkbox"></td>
						</c:if>
						<c:if test="${resource.is_check==0}">
							<td class="role_mtable_col1"><input type="checkbox"></td>
						</c:if>
						
						<c:if test="${resource.type==0}">
							<td class="role_mtable_col2 mtable_role_level_2" value="${resource.id }">${resource.res_name}</td>
						</c:if>
						<c:if test="${resource.type==1}">
							<td class="role_mtable_col2 mtable_role_level_3" value="${resource.id }">${resource.res_name}</td>
						</c:if>
						<c:if test="${resource.type==2}">
							<td class="role_mtable_col2 mtable_role_level_4" value="${resource.id }">${resource.res_name}</td>
						</c:if>
					</tr>
					<c:forEach var="kc" items="${resource.children}">
						<tr>
							<c:if test="${kc.is_check==1}">
								<td class="role_mtable_col1"><input checked="checked" type="checkbox"></td>
							</c:if>
							<c:if test="${kc.is_check==0}">
								<td class="role_mtable_col1"><input type="checkbox"></td>
							</c:if>
							
							<c:if test="${kc.type==0}">
								<td class="role_mtable_col2 mtable_role_level_2" value="${kc.id }">${kc.res_name}</td>
							</c:if>
							<c:if test="${kc.type==1}">
								<td class="role_mtable_col2 mtable_role_level_3" value="${kc.id }">${kc.res_name}</td>
							</c:if>
							<c:if test="${kc.type==2}">
								<td class="role_mtable_col2 mtable_role_level_4" value="${kc.id }">${kc.res_name}</td>
							</c:if>
						</tr>
					</c:forEach>
				</c:forEach>
			</table>
		</div>
	</div>

	<!--  Tab -->
	</form>
</body>
</html>