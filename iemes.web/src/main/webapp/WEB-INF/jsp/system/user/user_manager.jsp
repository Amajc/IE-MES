<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>	
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>用户管理</title>

<script type="text/javascript" src="js/common/form_validate.js"></script>
<script type="text/javascript" src="js/common/jtab.js"></script>
<script type="text/javascript" src="js/system/user/user_manager.js"></script>

</head>
<body>
<form class="mds_tab_form">
    <div id="commanButtonsDiv" class=iemes_style_commanButtonsDiv>
			<c:forEach var="resRow" items="${res}">
				<c:if test="${resRow.type==2 && resRow.is_hide!=1}">
					<c:choose>
						<c:when test="${resRow.res_url=='btnSave'}">
							<input type="submit" id="btnSave" value="${resRow.res_name}">
						</c:when>
						<c:otherwise>
							<input type="button" id="${resRow.res_url}"
								value="${resRow.res_name}">
						</c:otherwise>
					</c:choose>
				</c:if>
			</c:forEach>
	</div>
	<!-- <div id="commanButtonsDiv" class=iemes_style_commanButtonsDiv>
		<input type="button" id="btnQuery" value="检索"> 
		<input type="submit" id="btnSave"  value="保存"> 
		<input type="button" id="btnClear" value="清除"> 
		<input type="button" id="btnDelete" value="删除">
		<input type="button" id="btnResetPass" value="重置密码">
	</div> -->
	
	<div class="hidden">
		<input type="hidden" value="${page_res_id}" id="page_res_id">
		<input type="hidden" name="userFormMap.id" value="${user.id}" id="user_id">
		<input type="hidden" value="${errorMessage}" id="errorMessage">
		<input type="hidden" value="${successMessage}" id="successMessage">
	</div>

	<div id="pageNoticeInformation" class=iemes_style_NoticeDiv>
		 <label class="mds_message_label"></label>
		 <span class="iemes_style_NoticeButton"> <input type="button" submit="N" onclick="closeNoticeMessage();" value="关闭"> </span>
	</div>

	<div class="inputForm_query">
		<div class="formField_query">
			<label class="must">站点:</label> <span class="formText_query">${site}</span>
		</div>
		<div class="formField_query">
			<label class="must">用户编号:</label> 
			<input type="text"
				dataValue="account_name" 
				viewTitle="用户"
				data-url="/popup/queryAllUser.shtml" 
				class="formText_query majuscule" id="tbx_user_id" name="userFormMap.account_name" value="${user.account_name}" 
				validate_allowedempty="N" validate_errormsg="用户编号不能为空！" />
			<input type="button" submit="N" value="检索"  onclick="operationBrowse(this)"  textFieldId="tbx_user_id">
		</div>
	</div>


	<!--  Tab -->
	<ul id="tabs" class="tabs_li_2tabs">
		<li><a href="#" tabid="tab1">基础信息</a></li>
		<li><a href="#" tabid="tab2">角色绑定</a></li>
	</ul>
	<div id="content">
		<div id="tab1">
			<div class="inputForm_content">
				<div class="formField_content">
					<label class="must">用户名:</label> <input type="text"
						class="formText_content" id="tbx_username" name="userFormMap.user_name" value="${user.user_name}" 
						validate_allowedempty="N" validate_errormsg="用户名不能为空！"/>
				</div>
				<div class="formField_content">
					<label class="must">状态:</label> 
					<select class="formText_content"  id="select_isforbidden" name="userFormMap.locked">
					<c:if test="${user.locked==0}">
						<option value="0" selected="selected">正常</option>
						<option value="2">锁定</option>
					</c:if>
                    <c:if test="${user.locked==2}">
						<option value="0">正常</option>
						<option value="2" selected="selected">锁定</option>
					</c:if>
					<c:if test="${user==null}">
						<option value="0" selected="selected">正常</option>
						<option value="2">锁定</option>
					</c:if>
                    </select>
				</div>
				<div class="formField_content">
					<label>描述:</label> <input type="text"
						class="formText_content" id="tbx_user_des" name="userFormMap.description" value="${user.description }"/>
				</div>
				<div class="formField_content">
					<label>创建人:</label> <span class="formText_content"></span>
				</div>
				<div class="formField_content">
					<label>创建时间:</label> <span class="formText_content" value="${user.create_time}"></span>
				</div>
			</div>
		</div>
		<div id="tab2">
			<div class="inputForm_content">
					<table name="groups_tweenbox" class="groups_tweenTable">
						<tbody>
							<tr>
								<td>已绑定角色</td>
								<td></td>
								<td>可绑定角色</td>
							</tr>
							<tr>
								<td>
								<select id="selectGroups" multiple="multiple"
									name="userFormMap.roleSelectGroups">
									<c:forEach var="role" items="${bindRoleList}">
										<option value="${role.id }">${role.role_name}</option>
									</c:forEach>
								</select></td>
								<td>
									<div class="groups_tweenTable_centerDiv">
										<button onclick="selectedAll()" type="button" submit="N" title="全选">&lt;&lt;</button>
									</div>
									<div class="groups_tweenTable_centerDiv">
										<button onclick="selected()" type="button" submit="N" title="选择">&lt;</button>
									</div>
									<div class="groups_tweenTable_centerDiv">
										<button onclick="unselected()" type="button" submit="N" title="取消">&gt;</button>
									</div>
									<div class="groups_tweenTable_centerDiv">
										<button onclick="unselectedAll()" type="button"  submit="N" title="全取消">&gt;&gt;</button>
									</div>
								</td>
								<td><select id="groupsForSelect" multiple="multiple">
									<c:forEach var="role" items="${noBindRoleList}">
										<option value="${role.id }">${role.role_name}</option>
									</c:forEach>
								</select></td>
							</tr>
						</tbody>
					</table>
				</div>
		</div>
	</div>
	<!--  Tab -->
	</form>
</body>
</html>