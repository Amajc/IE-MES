<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>	
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>编号规则管理</title>

<script type="text/javascript" src="js/common/form_validate.js"></script>
<script type="text/javascript" src="js/common/jtab.js"></script>

<script type="text/javascript" src="js/workcenter_model/number_rule/number_rule_manager.js"></script>

</head>
<body>
	<form class="mds_tab_form">
		<div id="commanButtonsDiv" class=iemes_style_commanButtonsDiv>
			<c:forEach var="resRow" items="${res}">
				<c:if test="${resRow.type==2 && resRow.is_hide!=1}">
					<c:choose>
						<c:when test="${resRow.res_url=='btnSave'}">
							<input type="submit" id="btnSave" value="${resRow.res_name}">
						</c:when>
						<c:otherwise>
							<input type="button" id="${resRow.res_url}"
								value="${resRow.res_name}">
						</c:otherwise>
					</c:choose>
				</c:if>
			</c:forEach>
		</div>
		
		<div class="hidden">
			<input type="hidden" value="${page_res_id}" id="page_res_id">
			<input type="hidden" name="numberRuleFormMap.id" value="${numberRuleFormMap.id}" id="number_rule_id">
			<input type="hidden" value="${errorMessage}" id="errorMessage">
			<input type="hidden" value="${successMessage}" id="successMessage">
	    </div>

		<div id="pageNoticeInformation" class=iemes_style_NoticeDiv>
			<label class="mds_message_label"></label> <span
				class="iemes_style_NoticeButton"> <input type="button"
				submit="N" onclick="closeNoticeMessage();" value="关闭">
			</span>
		</div>

		<div class="inputForm_query">
			<div class="formField_query">
				<label class="must">站点:</label> <span class="formText_query">${site}</span>
			</div>
			<div class="formField_query">
			<label class="must">编号规则:</label>
			<input type="text"
			    dataValue="number_rule"
				viewTitle="编号规则"
				data-url="/popup/queryAllNumberRule.shtml" 
				class="formText_query majuscule" id="tbx_number_rule"
				name="numberRuleFormMap.number_rule" value="${numberRuleFormMap.number_rule}"
				validate_allowedempty="N" validate_errormsg="请输入编号规则！" />
		     <input type="button" submit="N" value="检索" onclick="operationBrowse(this)" textFieldId="tbx_number_rule">		
			</div>
		</div>


		<!--  Tab -->
		<ul id="tabs" class="tabs_li_1tabs">
			<li><a href="#" tabid="tab1">基础信息</a></li>
		</ul>
		<div id="content">
			<div id="tab1">
				<div class="inputForm_content">
					<div class="formField_content">
						<label>描述:</label> <input type="text" class="formText_content" 
						    name="numberRuleFormMap.number_rule_desc" value="${numberRuleFormMap.number_rule_desc}"
							id="tbx_number_rule_desc" />
					</div>
				    <div class="formField_content">
						<label>前缀:</label> <input type="text" class="formText_content"
						name="numberRuleFormMap.prefix" value="${numberRuleFormMap.prefix}"
							id="tbx_prefix" />
					</div>
					<div class="formField_content">
						<label>后缀:</label> <input type="text" class="formText_content"
						name="numberRuleFormMap.postfix" value="${numberRuleFormMap.postfix}"
							id="tbx_postfix" />
					</div>
					<div class="formField_content">
						<label class="must">基数:</label> 
						<input type="number" class="formText_content" 
							name="numberRuleFormMap.base_number" value="${numberRuleFormMap==null?0:numberRuleFormMap.base_number}" readonly="readonly"
							 id="tbx_base_num" validate_allowedempty="N" validate_datatype = "number_int" validate_errormsg="请输入基数(整数)！" />
					</div>
					<div class="formField_content">
						<label class="must">序列长度:</label> <input type="number" class="formText_content" readonly="readonly"
						validate_allowedempty="N" validate_datatype = "number_int" validate_errormsg="请输入序列长度(整数)！"
						name="numberRuleFormMap.order_length" value="${numberRuleFormMap==null?32:numberRuleFormMap.order_length}"
							id="tbx_order_length"/>
					</div>
					<div class="formField_content">
						<label class="must">增量:</label> <input type="number" class="formText_content" readonly="readonly"
						validate_allowedempty="N" validate_datatype = "number_int" validate_errormsg="请输入增量(整数)！"
						name="numberRuleFormMap.increment" value="${numberRuleFormMap==null?1:numberRuleFormMap.increment}"
							id="tbx_increment"/>
					</div>		
					<div class="formField_content">
						<label>编号示例:</label> <span class="formText_content">${numberRuleFormMap.number_example}</span>
					</div>
					<div class="formField_content">
						<label>创建人:</label> <span class="formText_content">${numberRuleFormMap.create_user}</span>
					</div>
					<div class="formField_content">
						<label>创建时间:</label> <span class="formText_content">${numberRuleFormMap.create_time}</span>
					</div>
					
				</div>
			</div>
		</div>

		<!--  Tab -->
	</form>
</body>
</html>