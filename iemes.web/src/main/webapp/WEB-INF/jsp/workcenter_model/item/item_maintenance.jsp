<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>物料维护</title>

<link rel="stylesheet" href="css/workcenter_model/item/item_maintenance.css" type="text/css" />
<script type="text/javascript" src="js/common/form_validate.js"></script>
<script type="text/javascript" src="js/common/jtab.js"></script>
<script type="text/javascript" src="js/common/mtable.js"></script>
<script type="text/javascript" src="js/workcenter_model/item/item_maintenance.js"></script>


</head>
<body>
<form class="mds_tab_form">
	<div id="commanButtonsDiv" class=iemes_style_commanButtonsDiv>
			<c:forEach var="resRow" items="${res}">
				<c:if test="${resRow.type==2 && resRow.is_hide!=1}">
					<c:choose>
						<c:when test="${resRow.res_url=='btnSave'}">
							<input type="submit" id="btnSave" value="${resRow.res_name}">
						</c:when>
						<c:otherwise>
							<input type="button" id="${resRow.res_url}"
								value="${resRow.res_name}">
						</c:otherwise>
					</c:choose>
				</c:if>
			</c:forEach>
	</div>
	
	<div class="hidden">
	    <input type="hidden" value="${page_res_id}" id="page_res_id">
		<input type="hidden" name="itemFormMap.id" value="${itemFormMap.id}" id="item_id">
		<input type="hidden" value="${errorMessage}" id="errorMessage">
		<input type="hidden" value="${successMessage}" id="successMessage">
	</div>

	<div id="pageNoticeInformation" class=iemes_style_NoticeDiv>
		 <label class="mds_message_label"></label>
		 <span class="iemes_style_NoticeButton"> <input type="button" submit="N" onclick="closeNoticeMessage();" value="关闭"> </span>
	</div>

	<div class="inputForm_query">
		<div class="formField_query">
			<label class="must">站点:</label> <span class="formText_query">${site}</span>
		</div>
		<div class="formField_query">
			<label class="must">物料编号:</label>
			
			<input type="text" class="formText_query majuscule"
				dataValue="item_no" 
				relationId= "tbx_item_version"
				viewTitle="物料"
				data-url="/popup/queryAllItem.shtml" 
				id="tbx_item_no" validate_allowedempty="N" validate_errormsg="物料编号不能为空！" name="itemFormMap.item_no" value="${itemFormMap.item_no }"/>
			<input type="button" submit="N" value="检索" onclick="operationBrowse(this)" textFieldId="tbx_item_no">
			
		</div>
	</div>


	<!--  Tab -->
	<ul id="tabs" class="tabs_li_3tabs">
		<li><a href="#" tabid="tab1">基础信息</a></li>
		<li><a href="#" tabid="tab2">替代品</a></li>
		<li><a href="#" tabid="tab3">自定义数据</a></li>
	</ul>
	<div id="content">
		<div id="tab1">
			<div class="inputForm_content">
				<div class="formField_content">
					<label class="must">物料名称:</label> <input type="text" 
						class="formText_content" id="tbx_item_name" name="itemFormMap.item_name" value="${itemFormMap.item_name }"
						validate_allowedempty="N" validate_errormsg="物料名称不能为空！" />
				</div>
				<div class="formField_content">
					<label>描述:</label> <input type="text"
						class="formText_content" id="tbx_item_des" name="itemFormMap.item_desc" value="${itemFormMap.item_desc }"/>
				</div>
				<div class="formField_content">
					<label >物料BOM:</label> 
					<input type="text" class="formText_query majuscule"
						dataValue="item_bom_no" 
						viewTitle="物料清单"
						relationId="input_item_id"
						data-url="/popup/queryAllItemBom.shtml" 
						id="tbx_itemBom_no" value="${itemBom.item_bom_no }"/>
					<input type="hidden" id="input_item_id" dataValue="id" relationId="item_bom_ver" name="itemFormMap.bom_no" value="${itemBom.id }">
					<input type="text" class="form_version majuscule" placeholder="版本" id="item_bom_ver" dataValue="item_bom_version" value="${itemBom.item_bom_version }">
					<input type="button" submit="N" value="检索"  onclick="operationBrowse(this)"  textFieldId="tbx_itemBom_no">
				</div>
				<div class="formField_content">
					<label class="must">版本:</label> 
					<input type="text" majuscule
						class="formText_content majuscule" id="tbx_item_version" name="itemFormMap.item_version" value="${itemFormMap.item_version }"
						dataValue="item_version" 
						validate_allowedempty="N" validate_errormsg="物料版本号必须输入一位字符！" maxlength="1"/>
				</div>
				<div class="formField_content">
					<label class="must">储存类型:</label> 
					<select class="formText_content"  id="select_item_save_type" name="itemFormMap.item_save_type">
						<c:if test="${itemFormMap==null}" >
							<option value="unit" selected="selected">个体</option>
	                    	<option value="batch">批次</option>
						</c:if>
	                    <c:if test="${itemFormMap.item_save_type=='unit'}" >
							<option value="unit" selected="selected">个体</option>
	                    	<option value="batch">批次</option>
						</c:if>
						<c:if test="${itemFormMap.item_save_type=='batch'}" >
							<option value="unit">个体</option>
	                    	<option value="batch" selected="selected">批次</option>
						</c:if>
                    </select>
				</div>
				<div class="formField_content">
					<label class="must">物料类型:</label> 
					<select class="formText_content"  id="select_item_type" name="itemFormMap.item_type">
						<c:if test="${itemFormMap==null}" >
							<option value="produce" selected="selected">自生产</option>
	                    	<option value="purchase">外采购</option>
						</c:if>
	                    <c:if test="${itemFormMap.item_type=='produce'}" >
							<option value="produce" selected="selected">自生产</option>
	                    	<option value="purchase">外采购</option>
						</c:if>
						<c:if test="${itemFormMap.item_type=='purchase'}" >
							<option value="produce">自生产</option>
	                    	<option value="purchase" selected="selected">外采购</option>
						</c:if>
                    </select>
				</div>
				<div class="formField_content">
					<label class="must">是否可用:</label> 
					<select class="formText_content"  id="select_isusable" name="itemFormMap.status">
						<c:if test="${itemFormMap==null}" >
		                    <option value="1" selected="selected">是</option>
		                    <option value="-1">否</option>
	                    </c:if>
						<c:if test="${itemFormMap.status==1}" >
		                    <option value="1" selected="selected">是</option>
		                    <option value="-1">否</option>
	                    </c:if>
	                    <c:if test="${itemFormMap.status==-1}" >
		                    <option value="1">是</option>
		                    <option value="-1" selected="selected">否</option>
	                    </c:if>
                    </select>
				</div>
				<div class="formField_content">
					<label>创建人:</label> <span class="formText_content">${itemFormMap.create_user }</span>
				</div>
				<div class="formField_content">
					<label>创建时间:</label> <span class="formText_content">${itemFormMap.create_time }</span>
				</div>	
			</div>
		</div>
		<div id="tab2">
			<div class="bt_div">
				<a class="bt_a" id="tb_add">插入新行</a>
				<a class="bt_a" id="tb_del">删除选定行</a>
				<a class="bt_a" id="tb_delAll">删除全部</a>
			</div>
			<table class="mtable" id="item_surrenal_Table">
				<tr>
					<th class="must" data-value="item_surrenal_level">顺序</th>
					<th class="must" data-value="item_surrenal_id">替代物料</th>
					<th class="must" data-value="item_surrenal_version">版本</th>
					<th class="must" data-value="item_surrenal_usage">有效装配物料</th>
					<th class="must" data-value="item_surrenal_usage_version">有效版本</th>
					<th class="must" data-value="start_validity_term">有效期自</th>
					<th class="must" data-value="end_validity_term">有效期至</th>
				</tr>
				<c:forEach var="item" items="${itemSurrenal}">
					<tr>
						<td>
					        <input type="text" readonly="readonly" validate_datatype="number_int" value="${item.item_surrenal_level }"
					        validate_errormsg="顺序必须为数字" value="10">
					    </td>
					    <td>
					        <input type="text" class="formText_query majuscule" datavalue="item_no" value="${item.item_surrenal_no }"
					        viewtitle="物料" data-url="/popup/queryAllItem.shtml" relationid="popup_item_surrenal_id10"
					        id="popup_item_surrenal_no10" validate_allowedempty="N" validate_errormsg="替代物料不能为空！">
					        <input type="hidden" id="popup_item_surrenal_id10" datavalue="id" relationid="item_surrenal_version10" value="${item.item_surrenal_id }">
					        <input type="button" submit="N" value="检索" onclick="operationBrowse(this)"
					        textfieldid="popup_item_surrenal_no10">
					    </td>
					    <td>
					        <input type="text" readonly="readonly" validate_allowedempty="N" id="item_surrenal_version10" value=${item.item_surrenal_version }
					        datavalue="item_version" validate_errormsg="版本不能为空！">
					    </td>
					    <td>
					        <input type="text" class="formText_query majuscule" datavalue="item_no" value="${item.item_surrenal_usage_no }"
					        viewtitle="物料" data-url="/popup/queryAllItem.shtml" relationid="item_surrenal_usage_id10"
					        id="item_surrenal_usage_no10" validate_allowedempty="N" validate_errormsg="有效装配物料不能为空！">
					        <input type="hidden" id="item_surrenal_usage_id10" datavalue="id" relationid="item_surrenal_usage_version10" value="${item.item_surrenal_usage }">
					        <input type="button" submit="N" value="检索" onclick="operationBrowse(this)"
					        textfieldid="item_surrenal_usage_no10">
					    </td>
					    <td>
					        <input type="text" readonly="readonly" validate_allowedempty="N" id="item_surrenal_usage_version10" value="${item.item_surrenal_usage_version }"
					        datavalue="item_version" validate_errormsg="有效版本不能为空！">
					    </td>
					    <td>
					        <input type="text" validate_allowedempty="N" 
					        datetime_format_type="datetime"
					        validate_errormsg="有效期自不能为空！" value="${item.start_validity_term }">
					    </td>
					    <td>
					        <input type="text" validate_allowedempty="N" 
					        datetime_format_type="datetime"
					        validate_errormsg="有效期至不能为空！" value="${item.end_validity_term }">
					    </td>
					</tr>
				</c:forEach>
			</table>
		</div>
		<div id="tab3">
			<table class="mtable" id="item_table">
				<tr>
					<th data-value="key">数据字段</th>
					<th data-value="value">字段值</th>
				</tr>
				<c:forEach var="key" items="${dataKeys}">
					<tr>
						<td data-value="${key.id}">${key.data_label}</td>
						<td data-value="${key.value}">
						   <input type='text' value="${key.value}">
						</td>
					</tr>
				</c:forEach>
			</table>
		</div>
	</div>

	<!--  Tab -->
	</form>
</body>
</html>