<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>		
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>操作维护</title>

<script type="text/javascript" src="js/common/form_validate.js"></script>
<script type="text/javascript" src="js/common/jtab.js"></script>
<script type="text/javascript" src="js/common/mtable.js"></script>
<script type="text/javascript" src="js/workcenter_model/operation/operation_maintenance.js"></script>

</head>
<body>
<form class="mds_tab_form">
	<div id="commanButtonsDiv" class=iemes_style_commanButtonsDiv>
			<c:forEach var="resRow" items="${res}">
				<c:if test="${resRow.type==2 && resRow.is_hide!=1}">
					<c:choose>
						<c:when test="${resRow.res_url=='btnSave'}">
							<input type="submit" id="btnSave" value="${resRow.res_name}">
						</c:when>
						<c:otherwise>
							<input type="button" id="${resRow.res_url}"
								value="${resRow.res_name}">
						</c:otherwise>
					</c:choose>
				</c:if>
			</c:forEach>
	</div>
	
	<div class="hidden">
		<input type="hidden" value="${page_res_id}" id="page_res_id">
		<input type="hidden" name="operationFormMap.id" value="${operation.id}" id="operation_id">
		<input type="hidden" value="${errorMessage}" id="errorMessage">
		<input type="hidden" value="${successMessage}" id="successMessage">
	</div>

	<div id="pageNoticeInformation" class=iemes_style_NoticeDiv>
		 <label class="mds_message_label"></label>
		 <span class="iemes_style_NoticeButton"> <input type="button" submit="N" onclick="closeNoticeMessage();" value="关闭"> </span>
	</div>

	<div class="inputForm_query">
		<div class="formField_query">
			<label class="must">站点:</label> <span class="formText_query">${site}</span>
		</div>
		<div class="formField_query">
			<label class="must">操作编号:</label> 
			<input type="text"
				dataValue="operation_no" 
				relationId= "tbx_opreation_version"
				viewTitle="操作"
				data-url="/popup/queryAllOperation.shtml" 
				class="formText_query majuscule" id="tbx_operation_no" value="${operation.operation_no }"
				validate_allowedempty="N" validate_errormsg="请输入操作编号！" name="operationFormMap.operation_no"/>
			<input type="button" submit="N" value="检索" onclick="operationBrowse(this)" textFieldId="tbx_operation_no">
		</div>
	</div>


	<!--  Tab -->
	<ul id="tabs" class="tabs_li_2tabs">
		<li><a href="#" tabid="tab1">基础信息</a></li>
		<li><a href="#" tabid="tab2">自定义数据</a></li>
	</ul>
	<div id="content">
		<div id="tab1">
			<div class="inputForm_content">		    
				<div class="formField_content">
					<label>描述:</label> <input type="text"
						class="formText_content" id="tbx_opreation_des" name="operationFormMap.operation_desc" value="${operation.operation_desc }"/>
				</div>
				<div class="formField_content">
					<label class="must">版本:</label> 
					<input type="text"
						class="formText_content majuscule" id="tbx_opreation_version"
						dataValue="operation_version" 
						validate_allowedempty="N" validate_errormsg="操作版本号必须输入一位字符！" maxlength="1" 
						name="operationFormMap.operation_version" value="${operation.operation_version }"/>
				</div>
				<div class="formField_content">
					<label class="must">是否可用:</label> 
					<select class="formText_content"  id="select_isusable" name="operationFormMap.status">
	                    <option <c:if test="${operation.status==1 }">selected="selected"</c:if> value="1">是</option>
                    	<option <c:if test="${operation.status==-1 }">selected="selected"</c:if> value="-1">否</option>
                    </select>
				</div>
				<div class="formField_content">
					<label class="must">操作类型:</label> 
					<select class="formText_content"  id="select_operation_type" name="operationFormMap.operation_type">
	                    <option <c:if test="${operation.operation_type=='normal' }">selected="selected"</c:if> value="normal">正常</option>
	                    <option <c:if test="${operation.operation_type=='test' }">selected="test"</c:if> value="test">测试</option>
	                    <option <c:if test="${operation.operation_type=='repair' }">selected="repair"</c:if> value="repair">维修</option>
                    </select>
				</div>
				<div class="formField_content">
					<label>默认资源:</label> 
					<input type="text"
				 		dataValue="resource_no" 
						viewTitle="资源"
						relationId="input_resource_id"
						data-url="/popup/queryAllResource.shtml"
						class="formText_content" id="tbx_default_work_resource" value="${resource.resource_no}"/>
					<input type="hidden" id="input_resource_id" dataValue="id" name="operationFormMap.default_resource" value="${resource.id }">
	                <input type="button" submit="N" value="检索" onclick="operationBrowse(this)" textFieldId="tbx_default_work_resource">
				</div>	
				<div class="formField_content">
					<label>最大循环次数:</label> 
					 <input type="number"
						class="formText_content" value="0" id="tbx_max_loop_times" name="operationFormMap.loop_num" maxlength="3"/>
				</div>	
				
				<div class="formField_content">
					<label>创建人:</label> <span class="formText_content">${operation.create_user}</span>
				</div>
				<div class="formField_content">
					<label>创建时间:</label> <span class="formText_content">${operation.create_time}</span>
				</div>
			</div>
		</div>
		<div id="tab2">
			<table class="mtable" id="operation_table">
				<tr>
					<th data-value="key">数据字段</th>
					<th data-value="value">字段值</th>
				</tr>
				<c:forEach var="key" items="${dataKeys}">
					<tr>
						<td data-value="${key.id}">${key.data_label}</td>
						<td data-value="${key.value}">
						   <input type='text' value="${key.value}">
						</td>
					</tr>
				</c:forEach>
			</table>
		</div>
	</div>

	<!--  Tab -->
	</form>
</body>
</html>