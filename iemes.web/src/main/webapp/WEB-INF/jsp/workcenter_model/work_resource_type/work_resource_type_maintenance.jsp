<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>			
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>资源类型维护</title>

<script type="text/javascript" src="js/common/form_validate.js"></script>
<script type="text/javascript" src="js/common/jtab.js"></script>

<script type="text/javascript" src="js/workcenter_model/work_resource_type/work_resource_type_maintenance.js"></script>

</head>
<body>
<form class="mds_tab_form">
	<div id="commanButtonsDiv" class=iemes_style_commanButtonsDiv>
			<c:forEach var="resRow" items="${res}">
				<c:if test="${resRow.type==2 && resRow.is_hide!=1}">
					<c:choose>
						<c:when test="${resRow.res_url=='btnSave'}">
							<input type="submit" id="btnSave" value="${resRow.res_name}">
						</c:when>
						<c:otherwise>
							<input type="button" id="${resRow.res_url}"
								value="${resRow.res_name}">
						</c:otherwise>
					</c:choose>
				</c:if>
			</c:forEach>
	</div>
	
	<div class="hidden">
		<input type="hidden" value="${page_res_id}" id="page_res_id">
		<input type="hidden" name="workResourceTypeFormMap.id" value="${workResourceTypeFormMap.id}" id="work_resource_type_id">
		<input type="hidden" value="${errorMessage}" id="errorMessage">
		<input type="hidden" value="${successMessage}" id="successMessage">
	</div>

	<div id="pageNoticeInformation" class=iemes_style_NoticeDiv>
		 <label class="mds_message_label"></label>
		 <span class="iemes_style_NoticeButton"> <input type="button" submit="N" onclick="closeNoticeMessage();" value="关闭"> </span>
	</div>

	<div class="inputForm_query">
	     <div class="formField_query">
			<label class="must">站点:</label> <span class="formText_query">${site}</span>
		</div>
		<div class="formField_query">
			<label class="must">资源类型:</label> 
			<input type="text"
				dataValue="resource_type"
				viewTitle="资源类型"
				data-url="/popup/queryAllResourceType.shtml"
				class="formText_query majuscule" id="tbx_work_resource_type" name="WorkResourceTypeFormMap.resource_type" value="${workResourceTypeFormMap.resource_type}"
				 validate_allowedempty="N" validate_errormsg="请输入资源类型！" />
			<input type="button" submit="N" value="检索" onclick="operationBrowse(this)" textFieldId="tbx_work_resource_type">
		</div>
	</div>


	<!--  Tab -->
	<ul id="tabs" class="tabs_li_2tabs">
		<li><a href="#" tabid="tab1">基础信息</a></li>
		<li><a href="#" tabid="tab2">资源分配</a></li>
	</ul>
	<div id="content">
		<div id="tab1">
			<div class="inputForm_content">
					<div class="formField_content">
						<label>描述:</label> <input type="text" class="formText_query"
						name="WorkResourceTypeFormMap.resource_type_des" value="${workResourceTypeFormMap.resource_type_des}"
							id="tbx_work_resource_type_des" />
					</div>
					<div class="formField_content">
						<label>创建人:</label> <span class="formText_content">${workResourceTypeFormMap.create_user}</span>
					</div>
					<div class="formField_content">
						<label>创建时间:</label> <span class="formText_content">${workResourceTypeFormMap.create_time}</span>
					</div>				
			</div>
		</div>
		<div id="tab2">
			<div class="inputForm_content">
					<table name="groups_tweenbox" class="groups_tweenTable">
						<tbody>
							<tr>
								<td>已分配资源</td>
								<td></td>
								<td>可分配资源</td>
							</tr>
							<tr>
								<td><select id="selectGroups" multiple="multiple"
									name="selectGroups">
										<c:forEach var="workResource" items="${selectedWorkResourceList}">
											<option value="${workResource.id }">${workResource.resource_no}</option>
										</c:forEach>
								</select></td>
								<td>
									<div class="groups_tweenTable_centerDiv">
										<button onclick="selectedAll()" type="button" submit="N" title="全选">&lt;&lt;</button>
									</div>
									<div class="groups_tweenTable_centerDiv">
										<button onclick="selected()" type="button" submit="N" title="选择">&lt;</button>
									</div>
									<div class="groups_tweenTable_centerDiv">
										<button onclick="unselected()" type="button" submit="N" title="取消">&gt;</button>
									</div>
									<div class="groups_tweenTable_centerDiv">
										<button onclick="unselectedAll()" type="button"  submit="N" title="全取消">&gt;&gt;</button>
									</div>
								</td>
								<td><select id="groupsForSelect" multiple="multiple">
										<c:forEach var="workResource" items="${unSelectedWorkResourceList}">
											<option value="${workResource.id }">${workResource.resource_no}</option>
										</c:forEach>
								</select></td>
							</tr>
						</tbody>
					</table>
				
			</div>
		</div>

	</div>

	<!--  Tab -->
	</form>
</body>
</html>