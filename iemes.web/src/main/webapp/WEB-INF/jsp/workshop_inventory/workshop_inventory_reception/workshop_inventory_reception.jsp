<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>	
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>车间库存接收</title>

<script type="text/javascript" src="js/common/form_validate.js"></script>
<script type="text/javascript" src="js/common/jtab.js"></script>

<script type="text/javascript" src="js/workshop_inventory/workshop_inventory_reception/workshop_inventory_reception.js"></script>

</head>
<body>
	<form class="mds_tab_form">
	    <!-- 最后再统一的改成动态按钮 -->
		<div id="commanButtonsDiv" class=iemes_style_commanButtonsDiv>
			<input type="submit" id="btnSave"  value="接收"> 
			<input type="button" id="btnClear" value="清除"> 
		</div>
		
		<div class="hidden">
		<input type="hidden" value="${errorMessage}" id="errorMessage">
		<input type="hidden" id="item_save_type" name="workshopInventoryFormMap.item_save_type">
		<input type="hidden" value="${successMessage}" id="successMessage">
    	</div>

		<div id="pageNoticeInformation" class=iemes_style_NoticeDiv>
		 <label class="mds_message_label"></label>
		 <span class="iemes_style_NoticeButton"> <input type="button" submit="N" onclick="closeNoticeMessage();" value="关闭"> </span>
		</div>

		<div class="inputForm_query">
			<div class="formField_query">
				<label class="must">站点:</label> <span class="formText_query">${site}</span>
			</div>
			<div class="formField_query">
				<label class="must">物料编号:</label> 
				<input type="text"
				       class="formText_query majuscule"
			           dataValue="item_no"
			           callBackFun="chooseItemSaveType"
			           relationId= "tbx_item_id"
			           viewTitle="物料"
			           data-url="/popup/queryAllItem.shtml" 
			           validate_allowedempty='N' validate_errormsg='物料编号不能为空！'
			           id="tbx_item_name"/>
				<input type="hidden" id="tbx_item_id" relationId= "tbx_item_version" dataValue="id"
				       name="workshopInventoryFormMap.item_id"/>
				<input type="text" class="form_version majuscule" placeholder="版本" id="tbx_item_version"
				       dataValue="item_version">
				<input type="button" submit="N" value="检索" onclick="operationBrowse(this)" textFieldId="tbx_item_name" />
			</div>
		</div>
		<!--  Tab -->
		<ul id="tabs" class="tabs_li_1tabs">
			<li><a href="#" tabid="tab1">接收信息</a></li>
		</ul>
		<div id="content">
			<div id="tab1">
				<div class="inputForm_content">
					<div class="formField_content">
						<label class="must" id="inventory_item">物料SFC:</label> 
						<input type="text" class="formText_content"
							name="workshopInventoryFormMap.item_sfc"
							validate_allowedempty="N" validate_errormsg="请输入物料SFC！"
							id="tbx_item_sfc" />
					</div>
					<div class="formField_query" id="div_item_receive_number">
						<label class="must" id="inventory_item">接收数量:</label> 
						<input type="text" class="formText_content"
							name="workshopInventoryFormMap.receive_number" value="1"
							validate_allowedempty="N" validate_errormsg="请输入接收数量！"
							id="tbx_item_receive_number" />
					</div>
					<div class="formField_query">
					<label class="must">车间:</label> 
					<input type="text"
						dataValue="workcenter_no"
						viewTitle="车间"
						relationId="input_workcenter_id"
						data-url="/popup/queryWorkCenter.shtml" 
						class="formText_query" id="tbx_workshop_no"/>
					<input type="hidden" id="input_workcenter_id" validate_allowedempty="N" validate_errormsg="车间不能为空！" relationId= "tbx_workshop_version"
					       dataValue="id" name="workshopInventoryFormMap.workshop_id" >	
					<input type="text" class="form_version majuscule" placeholder="版本" id="tbx_workshop_version"
					       dataValue="workcenter_version">
					<input type="button" submit="N" value="检索" onclick="operationBrowse(this)" textFieldId="tbx_workshop_no">
		      		</div>
					<div class="formField_content">
						<label>创建人:</label> <span class="formText_content">${workshopInventoryFormMap.create_user}</span>
					</div>
					<div class="formField_content">
						<label>创建时间:</label> <span class="formText_content">${workshopInventoryFormMap.create_time}</span>
					</div>
				</div>
			</div>

		</div>

		<!--  Tab -->
	</form>
</body>
</html>