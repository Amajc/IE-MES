<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
<title>SOP管理</title>

<script type="text/javascript" src="js/common/form_validate.js"></script>
<script type="text/javascript" src="js/common/jtab.js"></script>

<!-- ueditor start -->
<script type="text/javascript" src="./ueditor/ueditor.config.js"></script>
<!-- 编辑器源码文件 -->
<script type="text/javascript" src="./ueditor/ueditor.all.js"></script>
<!-- 语言包文件(建议手动加载语言包，避免在ie下，因为加载语言失败导致编辑器加载失败) -->
<script type="text/javascript" src="./ueditor/lang/zh-cn/zh-cn.js"></script>
<!-- ueditor end -->

<script type="text/javascript"
	src="js/work_scheduling/sop_manager/sop_manager.js"></script>

</head>
<body>
	<form class="mds_tab_form">
		<div id="commanButtonsDiv" class=iemes_style_commanButtonsDiv>
			<!-- 最后再统一的改成动态按钮 -->
			<input type="button" id="btnQuery" value="检索"> <input
				type="submit" id="btnSave" value="保存"> <input type="button"
				id="btnClear" value="清除">
		</div>

		<div id="pageNoticeInformation" class=iemes_style_NoticeDiv>
			<label class="mds_message_label"></label> <span
				class="iemes_style_NoticeButton"> <input type="button"
				submit="N" onclick="closeNoticeMessage();" value="关闭">
			</span>
		</div>

		<div class="hidden">
			<input type="hidden" name="sopFormMap.id" value="${sopFormMap.id}"
				id="sop_id"> <input type="hidden" value="${errorMessage}"
				id="errorMessage"> <input type="hidden"
				value="${successMessage}" id="successMessage">
		</div>

		<div class="inputForm_query">
			<%-- <div class="formField_query">
			<label class="must">站点:</label> <span class="formText_query">${site}</span>
		</div> --%>
			<div class="formField_query">
				<label>菜单:</label>
				<div class="hidden">
					<input type="hidden" id="resources_id"
						value="${sopFormMap.resources_id}">
				</div>
				<select class="formText_content" id="select_resources_id"
					name="sopFormMap.resources_id" value="${sopFormMap.resources_id}">
				</select>
			</div>
		</div>


		<!--  Tab -->
		<ul id="tabs" class="tabs_li_1tabs">
			<li><a href="#" tabid="tab1">SOP编辑</a></li>
		</ul>
		<div id="content">
			<div id="tab1">
				<div class="iemes_style_sop_div">
					<script id="container" name="content" type="text/plain"></script>
					<script type="text/javascript">
						//富文本框
						var ue = UE.getEditor('container', {
						    toolbars: [
						        ['source', 'undo', 'redo', 'bold', 'italic', 'underline', 'superscript', 'subscript', 'justifyleft', 
						        	'justifyright', 'justifycenter', 'justifyjustify', 'removeformat', 'cleardoc', 'autotypeset', 
						        	'fontfamily', 'forecolor', 'fontsize', 'link', 'paragraph', 'simpleupload', 'preview']],
						    initialFrameHeight : 400,
						    elementPathEnabled : false,
						    autoHeightEnabled: true,
						    autoFloatEnabled: true
						});
						
						ue.ready(function() {
							UE.getEditor('container').execCommand('insertHtml', '${sopFormMap.sop_content}');
						});
					</script>
				</div>
			</div>
		</div>

		<!--  Tab -->
	</form>
</body>
</html>