<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>	
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>工单下达</title>

<script type="text/javascript" src="js/common/form_validate.js"></script>
<script type="text/javascript" src="js/common/jtab.js"></script>

<script type="text/javascript" src="js/work_plan/shoporder_issuing/shoporder_issuing.js"></script>

</head>
<body>
<form class="mds_tab_form">
	<div id="commanButtonsDiv" class=iemes_style_commanButtonsDiv>
	    <%-- <c:forEach var="resRow" items="${res}">
				<c:if test="${resRow.type==2 && resRow.is_hide!=1}">
					<c:choose>
						<c:when test="${resRow.res_url=='btnSave'}">
							<input type="submit" id="btnSave" value="${resRow.res_name}">
						</c:when>
						<c:otherwise>
							<input type="button" id="${resRow.res_url}"
								value="${resRow.res_name}">
						</c:otherwise>
					</c:choose>
				</c:if>
	    </c:forEach> --%>
	    <!-- 最后再统一的改成动态按钮 -->
	    <input type="button" id="btnQuery" value="检索"> 
		<input type="submit" id="btnSave"  value="下达"> 
		<input type="button" id="btnClear" value="清除"> 
	</div>

    <div class="hidden">
		<input type="hidden" name="shoporderFormMap.id" value="${shoporderFormMap.id}" id="shoporder_id">
		<input type="hidden" value="${errorMessage}" id="errorMessage">
		<input type="hidden" value="${successMessage}" id="successMessage">
    </div>

	<div id="pageNoticeInformation" class=iemes_style_NoticeDiv>
		 <label class="mds_message_label"></label>
		 <span class="iemes_style_NoticeButton"> <input type="button" submit="N" onclick="closeNoticeMessage();" value="关闭"> </span>
	</div>

	<div class="inputForm_query">
		<div class="formField_query">
			<label class="must">站点:</label> <span class="formText_query">${site}</span>
		</div>
		<div class="formField_query">
			<label class="must">工单编号:</label>
			<input type="text"
			    dataValue="shoporder_no"
				viewTitle="工单"
				data-url="/popup/queryAllShoporder.shtml" 
				class="formText_query majuscule" id="tbx_shoporder_no" name="shoporderFormMap.shoporder_no" value="${shoporderFormMap.shoporder_no}"
				validate_allowedempty="N" validate_errormsg="请输入工单编号！" />
		     <input type="button" submit="N" value="检索" onclick="operationBrowse(this)" textFieldId="tbx_shoporder_no">		
		</div>
	</div>


	<!--  Tab -->
	<ul id="tabs" class="tabs_li_1tabs">
		<li><a href="#" tabid="tab1">工单下达操作</a></li>
	</ul>
	<div id="content">
		<div id="tab1">
			<div class="inputForm_content">		
			    <div class="formField_content">
					<label>状态:</label>  
					<c:forEach var="data" items="${shoporderStatusList}">
						    <c:if test="${shoporderFormMap.status==data.key}"><span class="formText_content">${data.value}</span></c:if>
					</c:forEach>
				</div>		
			    <div class="formField_content">
					<label>物料:</label> <span class="formText_content">${itemFormMap.item_name}</span>
				</div>
				<div class="formField_content">
					<label>物料版本:</label> <span class="formText_content">${itemFormMap.item_version}</span>
				</div>
				<div class="formField_content">
					<label>物料清单:</label> <span class="formText_content">${itemBomFormMap.item_bom_name}</span>
				</div>
				<div class="formField_content">
					<label>物料清单版本:</label> <span class="formText_content">${itemBomFormMap.item_bom_version}</span>
				</div>
				<div class="formField_content">
					<label>工艺路线:</label> <span class="formText_content">${processWorkFlowFormMap.process_workflow}</span>
				</div>	
				<div class="formField_content">
				    <label>工艺路线版本:</label> <span class="formText_content">${processWorkFlowFormMap.process_workflow_version}</span>
				</div>	
				<div class="formField_query">
					<label class="must">车间:</label> 
					<input type="text"
						dataValue="workcenter_no"
						viewTitle="车间"
						relationId="input_workcenter_id"
						data-url="/popup/queryWorkCenter.shtml" 
						class="formText_query" id="tbx_workshop_no"/>
					<input type="hidden" id="input_workcenter_id" validate_allowedempty="N" validate_errormsg="车间不能为空！" relationId= "tbx_workshop_version"
					       dataValue="id" name="shoporderFormMap.workshop_id" >	
					<input type="text" class="form_version majuscule" placeholder="版本" id="tbx_workshop_version"
					       dataValue="workcenter_version">
					<input type="button" submit="N" value="检索" onclick="operationBrowse(this)" textFieldId="tbx_workshop_no">
		       </div>
		       <div class="formField_query">
					<label class="must">产线:</label> 
					<input type="text"
						dataValue="workcenter_no"
						filterId="input_workcenter_id"
						relationId="input_workline_id"
						viewTitle="产线"
						data-url="/popup/queryWorkLine.shtml" 
						class="formText_query" id="tbx_workline_no"/>
					<input type="hidden" id="input_workline_id" validate_allowedempty="N" validate_errormsg="产线不能为空！" relationId= "tbx_workline_version"
					       dataValue="id" name="shoporderFormMap.workline_id">	
					<input type="text" class="form_version majuscule" placeholder="版本" id="tbx_workline_version"
					       dataValue="workcenter_version">
					<input type="button" submit="N" value="检索" onclick="operationBrowse(this)" textFieldId="tbx_workline_no">
		       </div>
		       <div class="formField_query">
					<label>编号规则:</label> 
					<input type="text"
					    dataValue="number_rule"
					    relationId="input_numble_rule_id"
						viewTitle="编号规则"
						data-url="/popup/queryAllNumberRule.shtml" 
						class="formText_query majuscule" id="tbx_number_rule"
						name="shoporderFormMap.number_rule" />
					<input type="hidden" id="input_numble_rule_id"
					       dataValue="id" name="shoporderFormMap.number_rule_id">	
					<input type="button" submit="N" value="检索" onclick="operationBrowse(this)" textFieldId="tbx_number_rule">
		       </div>
				<div class="formField_content">
					<label>已下达数量:</label> <span class="formText_content">${shoporderFormMap.shoporder_issued_number}</span>
				</div>
				<div class="formField_content">
					<label>完成数量:</label> <span class="formText_content">${shoporderFormMap.completed_number}</span>
				</div>
				<div class="formField_content">
					<label>报废数量:</label> <span class="formText_content">${shoporderFormMap.scrap_number}</span>
				</div>
				<div class="formField_content">
					<label>可下达数量:</label> <span class="formText_content">${shoporderFormMap.can_issuing_number}</span>
				</div>
				<div class="formField_content">
					<label class="must">下达数量:</label> <input type="text"
					name="shoporderFormMap.issuing_number" value="${shoporderFormMap.issuing_number}"
					validate_allowedempty="N" validate_datatype = "number_int" validate_errormsg="请输入下达数量(整数)！"
						class="formText_content" id="tbx_shop_order_issuing_number" />
				</div>				
		    </div>
		</div>
	</div>

	<!--  Tab -->
	</form>
</body>
</html>