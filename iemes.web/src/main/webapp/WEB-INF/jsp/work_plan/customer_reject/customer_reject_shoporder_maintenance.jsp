<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link rel="shortcut icon"
	href="${pageContext.servletContext.contextPath }/images/favicon.png"
	type="image/x-icon" />
<title>客退单</title>

<script type="text/javascript" src="js/common/form_validate.js"></script>
<script type="text/javascript" src="js/common/jtab.js"></script>
<script type="text/javascript" src="js/common/mtable.js"></script>

<script type="text/javascript"
	src="js/work_plan/customer_reject/customer_reject_shoporder_maintenance.js"></script>

</head>
<body>
	<form class="mds_tab_form">
		<div id="commanButtonsDiv" class=iemes_style_commanButtonsDiv>
			<c:forEach var="resRow" items="${res}">
				<c:if test="${resRow.type==2 && resRow.is_hide!=1}">
					<c:choose>
						<c:when test="${resRow.res_url=='btnSave'}">
							<input type="submit" id="btnSave" value="${resRow.res_name}">
						</c:when>
						<c:otherwise>
							<input type="button" id="${resRow.res_url}"
								value="${resRow.res_name}">
						</c:otherwise>
					</c:choose>
				</c:if>
			</c:forEach>
		</div>

		<div class="hidden">
			<input type="hidden" value="${page_res_id}" id="page_res_id">
			<input type="hidden" name="shoporderFormMap.id" value="${shoporderFormMap.id}" id="shoporder_id"> 
			<input type="hidden" value="${errorMessage}" id="errorMessage">
			<input type="hidden" value="${successMessage}" id="successMessage">
			<input type="hidden" name="shoporderFormMap.shoporder_type" value="rework" id="shoporder_type">
			<input type="hidden" value="rework" id="process_flow_type">
			<input type="hidden" id="check_shoporder_id">
		</div>

		<div id="pageNoticeInformation" class=iemes_style_NoticeDiv>
			<label class="mds_message_label"></label> 
			<span class="iemes_style_NoticeButton"> 
				<input type="button" submit="N" onclick="closeNoticeMessage();" value="关闭">
			</span>
		</div>

		<div class="inputForm_query">
			<div class="formField_query">
				<label class="must">站点:</label> <span class="formText_query">${site}</span>
			</div>
			<div class="formField_query">
				<label class="must">客退单编号:</label> 
				<input type="text"
					dataValue="shoporder_no" viewTitle="客退单" filterId="shoporder_type"
					data-url="/popup/queryShoporderByType.shtml"
					class="formText_query majuscule" id="tbx_shoporder_no"
					name="shoporderFormMap.shoporder_no"
					value="${shoporderFormMap.shoporder_no}" validate_allowedempty="N"
					validate_errormsg="请输入客退单编号！" /> 
				<input type="button" submit="N" value="检索" onclick="operationBrowse(this)" textFieldId="tbx_shoporder_no">
			</div>
		</div>

		<!--  Tab -->
		<ul id="tabs" class="tabs_li_2tabs">
			<li><a href="#" tabid="tab1">基础信息</a></li>
			<li><a href="#" tabid="tab2">客退信息关联</a></li>
		</ul>
		<div id="content">
			<div id="tab1">
				<div class="inputForm_content">
					<div class="formField_content">
						<label>状态:</label>
						<c:forEach var="data" items="${shoporderStatusList}">
							<c:if test="${shoporderFormMap.status==data.key}">
								<span class="formText_content">${data.value}</span>
							</c:if>
						</c:forEach>
					</div>
					<div class="formField_content">
						<label class="must">工艺路线:</label> 
						<input type="text"
							class="formText_query majuscule" dataValue="process_workflow" filterId="process_flow_type"
							relationId="tbx_shoporder_process_workflow_id" viewTitle="工艺路线"
							data-url="/popup/queryProcessWorkflowByType.shtml"
							validate_allowedempty='N' validate_errormsg='工艺路线不能为空！'
							id="tbx_process_workflow_name"
							value="${processWorkFlowFormMap.process_workflow }" /> 
						<input
							type="hidden" id="tbx_shoporder_process_workflow_id"
							relationId="tbx_process_workflow_version" dataValue="id"
							name="shoporderFormMap.process_workflow_id"
							value="${processWorkFlowFormMap.id }" /> 
						<input type="text"
							class="form_version majuscule" placeholder="版本"
							id="tbx_process_workflow_version"
							dataValue="process_workflow_version"
							value="${processWorkFlowFormMap.process_workflow_version }">
						<input type="button" submit="N" value="检索"
							onclick="operationBrowse(this)"
							textFieldId="tbx_process_workflow_name" />
					</div>
					<div class="formField_content">
						<label>计划开始时间:</label> <input type="text"
							datetime_format_type="datetime"
							name="shoporderFormMap.shoporder_plan_start_time"
							value="${shoporderFormMap.shoporder_plan_start_time}"
							class="formText_content" id="tbx_shop_order_plan_start_time" />
					</div>
					<div class="formField_content">
						<label>计划结束时间:</label> <input type="text"
							datetime_format_type="datetime"
							name="shoporderFormMap.shoporder_plan_end_time"
							value="${shoporderFormMap.shoporder_plan_end_time}"
							class="formText_content" id="tbx_shop_order_plan_end_time" />
					</div>
					<div class="formField_content">
						<label>创建人:</label> <span class="formText_content">${shoporderFormMap.create_user}</span>
					</div>
					<div class="formField_content">
						<label>创建时间:</label> <span class="formText_content">${shoporderFormMap.create_time}</span>
					</div>
				</div>
			</div>
			<div id="tab2">
				<div class="bt_div">
					<a class="bt_a" id="tb_add">插入新行</a>
					<a class="bt_a" id="tb_del">删除选定行</a>
					<a class="bt_a" id="tb_delAll">删除全部</a>
				</div>
				<table class="mtable" id="customerRejectSfcTable">
					<tr>
						<th data-value="old_sfc">原SFC</th>
						<th data-value="new_sfc">新SFC</th>
						<th class="mtable_head_hide" data-value="old_shoporder_id">旧工单</th>
					</tr>
					<c:forEach var="customerRejectSfcInfoFormMap" items="${listCustomerRejectSfcInfoFormMap}">
						<tr>
							<td><input type='text' value="${customerRejectSfcInfoFormMap.old_sfc }"></td>
							<td><input type='text' value="${customerRejectSfcInfoFormMap.new_sfc }"></td>
						</tr>
					</c:forEach>
				</table>
			</div>
		</div>

		<!--  Tab -->
	</form>
</body>
</html>