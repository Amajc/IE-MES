<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>进度报表</title>
</head>
<body>
	<form class="mds_tab_form">
		<div class="hidden">
			<input type="hidden" name="formMap.reportMethod" value="getProductionRecordList"> 
			<input type="hidden" value="${errorMessage}" id="errorMessage"> 
			<input type="hidden" value="${successMessage}" id="successMessage">
		</div>

		<div id="pageNoticeInformation" class=iemes_style_NoticeDiv>
			<label class="mds_message_label"></label> <span
				class="iemes_style_NoticeButton"> <input type="button"
				submit="N" onclick="closeNoticeMessage();" value="关闭">
			</span>
		</div>
	
		<div id="commanButtonsDiv" class="iemes_style_commanButtonsDiv">
			<input type="button" id="btnQuery" value="检索">
			<input type="button" id="btnQuery" value="清除">
		</div>
		
		<div class="mds_container">
			<div class="mds_row-20"></div>
			<div class="mds_row content_search_button">
				<div class="col-md-3 col-sm-3">
					<label>送测日期</label> 
					<input type="text" id="input_begin_time" name="formMap.begin_time" style="width: 140px; margin-right: 40px">
				</div>
				<div class="col-md-3 col-sm-3">
					<label>报告编号</label> 
					<input type="text" id="input_begin_time" name="formMap.begin_time" style="width: 140px; margin-right: 40px">
				</div>
				<div class="col-md-3 col-sm-3">
					<label>送测部门</label> 
					<input type="text" id="input_begin_time" name="formMap.begin_time" style="width: 140px; margin-right: 40px">
				</div>
				<div class="col-md-3 col-sm-3">
					<label>送测人</label> 
					<input type="text" id="input_begin_time" name="formMap.begin_time" style="width: 140px; margin-right: 40px">
				</div>
			</div>
			<div class="mds_row content_search_button">
				<div class="col-md-3 col-sm-3">
					<label>颜色</label> 
					<input type="text" id="input_begin_time" name="formMap.begin_time" style="width: 140px; margin-right: 40px">
				</div>
				<div class="col-md-3 col-sm-3">
					<label>供应商</label> 
					<input type="text" id="input_begin_time" name="formMap.begin_time" style="width: 140px; margin-right: 40px">
				</div>
				<div class="col-md-3 col-sm-3">
					<label>送测数量</label> 
					<input type="text" id="input_begin_time" name="formMap.begin_time" style="width: 140px; margin-right: 40px">
				</div>
				<div class="col-md-3 col-sm-3">
					<label>生产工艺</label> 
					<input type="text" id="input_begin_time" name="formMap.begin_time" style="width: 140px; margin-right: 40px">
				</div>
			</div>
			<div class="mds_row content_search_button">
				<div class="col-md-3 col-sm-3">
					<label>测试状态</label> 
					<input type="text" id="input_begin_time" name="formMap.begin_time" style="width: 140px; margin-right: 40px">
				</div>
			</div>
		</div>
		<div class="mds_row-20"></div>
		<div id="production_record_report">
			<div class="openPopup_title">进度报表</div>
			<table class="mtable" id="production_record_table">
				<tr>
					<th data-value="shoporder_no">送测日期</th>
					<th data-value="sfc">报告编号</th>
					<th data-value="sfc_status">送测部门</th>
					<th data-value="main_item">送测人</th>
					<th data-value="shoporder_status">颜色</th>
					<th data-value="workshop_no">油漆供应商</th>
					<th data-value="workline_no">送测数量</th>
					<th data-value="shoporder_create_time">生产工艺</th>
					<th data-value="shoporder_create_user">测试状态</th>
					<th data-value="more_info">铅笔硬度测试</th>
					<th data-value="more_info">附着力测试</th>
					<th data-value="more_info">RCA耐磨测试</th>
					<th data-value="more_info">振动耐磨测试</th>
					<th data-value="more_info">酒精耐磨测试</th>
					<th data-value="more_info">抗脏污测试</th>
					<th data-value="more_info">盐雾</th>
					<th data-value="more_info">低温测试</th>
					<th data-value="more_info">高温测试</th>
					<th data-value="more_info">交变湿热</th>
					<th data-value="more_info">温度冲击</th>
					<th data-value="more_info">太阳辐射</th>
					<th data-value="more_info">钢丝绒测试</th>
					<th data-value="more_info">水煮测试</th>
					<th data-value="more_info">耐手汗测试</th>
					<th data-value="more_info">耐化妆品测试</th>
					<th data-value="more_info">落锤测试</th>
					<th data-value="more_info">弯折测试</th>
					<th data-value="more_info">最终测试结果</th>
					<th data-value="more_info">测试报告</th>
					<th data-value="more_info">备注</th>
				</tr>
				<tr>
					<td>2017/9/13</td>
					<td>002</td>
					<td>项目</td>
					<td>翟玮</td>
					<td>金色</td>
					<td>施美克</td>
					<td>48</td>
					<td>喷涂</td>
					<td>已完成</td>
					<td>OK</td>
					<td>OK</td>
					<td>OK</td>
					<td>OK</td>
					<td>OK</td>
					<td>OK</td>
					<td>OK</td>
					<td>OK</td>
					<td>OK</td>
					<td>OK</td>
					<td>OK</td>
					<td></td>
					<td></td>
					<td>OK</td>
					<td>OK</td>
					<td>OK</td>
					<td>OK</td>
					<td>OK</td>
					<td>OK</td>
					<td><a style="text-decoration: underline;color: #06c;" onclick="showReport()">查看报告</a></td>
					<td></td>
				</tr>
				<tr>
					<td>2017/9/13</td>
					<td>003</td>
					<td>项目</td>
					<td>翟玮</td>
					<td>金色</td>
					<td>海明德</td>
					<td>45</td>
					<td>喷涂</td>
					<td>已完成</td>
					<td>OK</td>
					<td>NG</td>
					<td>OK</td>
					<td>OK</td>
					<td>OK</td>
					<td>OK</td>
					<td>OK</td>
					<td>OK</td>
					<td>OK</td>
					<td>OK</td>
					<td>OK</td>
					<td></td>
					<td></td>
					<td>OK</td>
					<td>OK</td>
					<td>OK</td>
					<td>OK</td>
					<td>OK</td>
					<td>OK</td>
					<td><a style="text-decoration: underline;color: #06c;" onclick="showReport()">查看报告</a></td>
					<td></td>
				</tr>
				<tr>
					<td>2017/9/13</td>
					<td>009</td>
					<td>项目</td>
					<td>王运平</td>
					<td>金色</td>
					<td>施美克</td>
					<td>40</td>
					<td>喷涂</td>
					<td>已完成</td>
					<td>OK</td>
					<td>OK</td>
					<td>NG</td>
					<td>OK</td>
					<td>OK</td>
					<td>OK</td>
					<td>OK</td>
					<td>OK</td>
					<td>OK</td>
					<td>OK</td>
					<td>OK</td>
					<td></td>
					<td></td>
					<td>OK</td>
					<td>OK</td>
					<td>OK</td>
					<td>OK</td>
					<td>OK</td>
					<td>OK</td>
					<td><a style="text-decoration: underline;color: #06c;" onclick="showReport()">查看报告</a></td>
					<td></td>
				</tr>
				<tr>
					<td>2017/9/13</td>
					<td>110</td>
					<td>项目</td>
					<td>翟玮</td>
					<td>金色</td>
					<td>施美克</td>
					<td>75</td>
					<td>喷涂</td>
					<td>已完成</td>
					<td>OK</td>
					<td>OK</td>
					<td>OK</td>
					<td>OK</td>
					<td>OK</td>
					<td>OK</td>
					<td>OK</td>
					<td>OK</td>
					<td>OK</td>
					<td>OK</td>
					<td>OK</td>
					<td></td>
					<td></td>
					<td>OK</td>
					<td>OK</td>
					<td>OK</td>
					<td>OK</td>
					<td>OK</td>
					<td>OK</td>
					<td><a style="text-decoration: underline;color: #06c;" onclick="showReport()">查看报告</a></td>
					<td></td>
				</tr>
				<tr>
					<td>2017/9/13</td>
					<td>118</td>
					<td>项目</td>
					<td>翟玮</td>
					<td>黑色</td>
					<td>施美克</td>
					<td>36</td>
					<td>喷涂</td>
					<td>已完成</td>
					<td>OK</td>
					<td>OK</td>
					<td>OK</td>
					<td>OK</td>
					<td>OK</td>
					<td>OK</td>
					<td>OK</td>
					<td>OK</td>
					<td>OK</td>
					<td>OK</td>
					<td>OK</td>
					<td></td>
					<td></td>
					<td>OK</td>
					<td>OK</td>
					<td>OK</td>
					<td>OK</td>
					<td>OK</td>
					<td>OK</td>
					<td><a style="text-decoration: underline;color: #06c;" onclick="showReport()">查看报告</a></td>
					<td></td>
				</tr>
			</table>
		</div>
	</form>
</body>
</html>