<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

<link rel="stylesheet" href="css/about.css" type="text/css" />

<title>About</title>
</head>

<body>
	<div class="about_show">
		<div class="about_show_header">
		   <div class="welcome_show_header1"><span>关于我们</span></div>
	    </div>		
	    <div class="about_show_content">
		   <!-- <p>深圳市迈鼎盛信息技术有限公司，坐落在龙岗创新创业基地-硅谷电子商务港。</p>
		   <p>是一家专注于企业信息化管理软件的咨询、服务公司。</p>
		   <p>我们提供MES、WMS、OA、供应链为核心的企业管理软件整体解决方案，</p>
		   <p>并开拓有移动APP设计与开发、微信公众号小程序开发运营、</p>
		   <p>H5网站设计与开发等移动新兴业务。   </p> -->
		   <p>&nbsp;&nbsp;&nbsp;&nbsp;深圳市迈鼎盛信息技术有限公司，
		           坐落在龙岗创新创业基地-硅谷电子商务港。
		           是一家专注于企业信息化管理软件的咨询、服务公司。我们提供MES、WMS、OA、供应链为核心的企业管理软件整体解决方案，
		           并开拓有移动APP设计与开发、微信公众号小程序开发运营、H5网站设计与开发等移动新兴业务。</p>
		   <p>&nbsp;&nbsp;&nbsp;&nbsp;迈鼎盛团队核心成员毕业于武汉理工、中国地质大学、华中科技大学等知名高校，
		           并在比亚迪、华为、日月光等知名企业任职，在企业信息化领域积累有丰富的经验和实战能力。</p>
		   <!-- <p>&nbsp;&nbsp;&nbsp;&nbsp;公司官网:<a href="http://www.maidingsheng.net">http://www.maidingsheng.net</a></p> -->
		   <p>&nbsp;&nbsp;&nbsp;&nbsp;公司官网:http://www.maidingsheng.net</p>
	    </div>	 
	    
	    <div class="about_show_foot">
			<span>版权所有 ©2017 迈鼎盛。保留所有权利</span>		    
	    </div>	 
	</div>
</body>
</html>