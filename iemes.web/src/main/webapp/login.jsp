<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html lang="en"
	class="app js no-touch no-android chrome no-firefox no-iemobile no-ie no-ie10 no-ie11 no-ios no-ios7 ipad">
<head>
<style type="text/css">
</style>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- Google Chrome Frame也可以让IE用上Chrome的引擎: -->
<meta http-equiv="X-UA-Compatible" content="IE=edge;chrome=1">
<link rel="shortcut icon"
	href="${pageContext.servletContext.contextPath }/images/favicon.png"
	type="image/x-icon" />
<meta name="renderer" content="webkit">
<title>IE-MES制造执行系统</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<link rel="stylesheet" href="css/login.css" type="text/css" />

</head>
<body>
	<div class="login_body">
		<div class="login_container">
			<div class="login_pic">
				<img class="login_img" alt="" src="images/branding-image.jpg">
			</div>
			<div class="login_centent">
				<div class="login_logo"></div>
				<form
					action="${pageContext.servletContext.contextPath }/login.shtml"
					method="post" class="login_form">

					<div class="login_form_row login_message">
						<label id="login_message">${error}</label>
					</div>
					<div class="login_form_row">
						<label class="login_label">用户：</label> <input class="login_input"
							type="text" name="username">
					</div>
					<div class="login_form_row">
						<label class="login_label">密码：</label> <input class="login_input"
							type="password" name="password">
					</div>
					<div class="login_form_row">
						<label class="login_label">站点：</label> <select class="login_site"
							name="site">
							<c:forEach items="${siteInfo}" var="key">
								<option value="${key.id}">${key.site_name}</option>
							</c:forEach>
						</select>
					</div>
					<div class="login_form_row login_bt">
						<input type="submit" value="登录">
					</div>
				</form>
				<div class="login_foot">© 2018 www.maidingsheng.net 版权所有</div>
			</div>
		</div>
	</div>
</body>
</html>