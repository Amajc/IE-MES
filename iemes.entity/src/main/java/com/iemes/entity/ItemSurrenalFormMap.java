package com.iemes.entity;

import com.iemes.entity.annotation.TableSeg;
import com.iemes.entity.FormMap;

/**
 * 有效替代物料
 * @author Administrator
 *
 */
@TableSeg(tableName = "mds_item_surrenal", id="id")
public class ItemSurrenalFormMap extends FormMap<String,Object> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 9194243089594808161L;

}
