package com.iemes.entity;

import com.iemes.entity.annotation.TableSeg;
import com.iemes.entity.FormMap;

/**
 * 维修记录表
 * 扩展FormMap
 * @author Administrator
 *
 */
@TableSeg(tableName = "mds_nc_repair", id="id")
public class NcRepairFormMap extends FormMap<String,Object>{

	private static final long serialVersionUID = 1L;

}
