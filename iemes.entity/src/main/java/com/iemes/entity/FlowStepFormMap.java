package com.iemes.entity;

import com.iemes.entity.annotation.TableSeg;
import com.iemes.entity.FormMap;



/**
 * mds_flow_step实体表
 */
@TableSeg(tableName = "mds_flow_step", id="id")
public class FlowStepFormMap extends FormMap<String,Object>{

	/**
	 * 
	 */
	private static final long serialVersionUID = 5385622400095935760L;


}
