package com.iemes.entity;

import com.iemes.entity.annotation.TableSeg;
import com.iemes.entity.FormMap;



/**
 * 实体表
 */
@TableSeg(tableName = "ly_exam", id="id")
public class ExamInfoFormMap extends FormMap<String,Object>{

	/**
	 *@descript
	 *@author MDS
	 *@version 2.0
	 */
	private static final long serialVersionUID = 1L;

}
