package com.iemes.service.work_plan;

import java.util.List;

import com.iemes.entity.FlowStepFormMap;
import com.iemes.entity.NumberRuleFormMap;
import com.iemes.entity.ShopOrderSfcFormMap;
import com.iemes.entity.ShoporderFormMap;
import com.iemes.exception.BusinessException;

public interface ShoporderIssuingService {
	
	/**
	 * 下达工单
	 * @param shoporderFormMap...
	 * @throws BusinessException
	 */
	void issuingShoporder(ShoporderFormMap shoporderFormMap, ShoporderFormMap shoporderFormMap2,
			NumberRuleFormMap numberRuleFormMap, FlowStepFormMap flowStepFormMap, List<String> listSfcNo,
			List<ShopOrderSfcFormMap> listExsitedShopOrderSfcFormMap)throws BusinessException;
}
