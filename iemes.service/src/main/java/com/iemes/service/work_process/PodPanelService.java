package com.iemes.service.work_process;

import com.iemes.entity.PodPanelFormMap;
import com.iemes.exception.BusinessException;

public interface PodPanelService {

	/**
	 * 保存POD面板
	 * @param podButtonFormMap
	 * @throws BusinessException
	 */
	void savePodPanel(PodPanelFormMap podButtonFormMap)throws BusinessException;

	/**
	 * 删除POD面板
	 * @param podButtonId
	 * @throws BusinessException
	 */
	void delPodPanel(String podButtonId)throws BusinessException;
}
