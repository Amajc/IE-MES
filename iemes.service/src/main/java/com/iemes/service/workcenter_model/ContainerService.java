package com.iemes.service.workcenter_model;

import com.iemes.entity.ContainerTypeFormMap;
import com.iemes.exception.BusinessException;

public interface ContainerService {

	/**
	 * 保存容器信息
	 * @param containerFormMap
	 * @throws BusinessException
	 */
	void saveContainer(ContainerTypeFormMap containerFormMap)throws BusinessException;

	/**
	 * 删除容器信息
	 * @param containerId
	 * @throws BusinessException
	 */
	void delContainer(String containerId)throws BusinessException;
}
