package com.iemes.service.workcenter_model;

import com.iemes.entity.WorkResourceFormMap;
import com.iemes.exception.BusinessException;

public interface WorkResourceService {
	
	/**
	 * 保存资源
	 * @param workResourceFormMap
	 * @throws BusinessException
	 */
	void saveWorkResource(WorkResourceFormMap workResourceFormMap)throws BusinessException;

	/**
	 * 删除资源
	 * @param workResourceId
	 * @throws BusinessException
	 */
	void delWorkResource(String workResourceId)throws BusinessException;
}
